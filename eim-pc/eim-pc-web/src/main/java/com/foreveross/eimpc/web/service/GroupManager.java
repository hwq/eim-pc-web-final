package com.foreveross.eimpc.web.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.foreveross.eimpc.web.constants.Configs;
import com.foreveross.eimpc.web.util.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

public class GroupManager {

    public static Logger logger = LoggerFactory.getLogger(GroupManager.class);

    /**
     *
     * 创建群聊
     *
     * @param usernameStr 成员
     * @param groupName 群聊名称
     * @param creator 创建人
     * @param access_token token
     * @return
     */
	public static String createGroup(String usernameStr, String groupName, String creator, String access_token){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", groupName);
        jsonObject.put("accountUid", creator);
		String[] usernames = usernameStr.split(",");
		JSONArray jsonArray = new JSONArray();
		JSONObject tempJsonObject = null;
		for(int i=0;i<usernames.length;i++){
			tempJsonObject = new JSONObject();
			tempJsonObject.put("accountUid", usernames[i]);
			jsonArray.add(tempJsonObject);
		}
		jsonObject.put("members", jsonArray);
		String url = MessageFormat.format(Configs.GROUP_CREATE_URL, access_token);
		return HttpClientUtil.post(url, jsonObject.toJSONString());
	}

    /**
     * 查询群组信息
     * @param groupId 群聊ID
     * @param access_token token
     * @return
     */
	public static String queryGroup(String groupId, String access_token){
		String url = MessageFormat.format(Configs.GROUP_QUERY_URL, groupId, access_token);
		return HttpClientUtil.get(url);
	}

    /**
     *
     * @param groupIdentifier 群组编号
     * @param opAccountUid 邀请人uid
     * @param addAccountUids 所要加群的成员Uid
     * @param access_token 当前登录用户访问令牌
     * @return
     */
    public static String addContact(String groupIdentifier, String opAccountUid, String addAccountUids, String access_token){
        JSONArray jsonArray = new JSONArray();
        for(String accountUid : addAccountUids.split(",")){
            jsonArray.add(accountUid);
        }
        String url = MessageFormat.format(Configs.GROUP_ADD_CONTACT_URL, groupIdentifier, opAccountUid, access_token);
        System.out.println(jsonArray.toJSONString());
        return  HttpClientUtil.post(url, jsonArray.toJSONString());
    };


    /**
     * 群组踢人
     * @param groupIdentifier 群聊ID
     * @param opAccountUid 操作人ID
     * @param exitAccountUid 退出人ID
     * @param access_token token
     * @return
     */
    public static String exit(String groupIdentifier, String opAccountUid, String exitAccountUid, String access_token){
        String url = MessageFormat.format(Configs.GROUP_EXIT_URL, groupIdentifier, opAccountUid, exitAccountUid, access_token);
        return HttpClientUtil.get(url);
    }

    /**
     * 修改群聊信息
     * @param groupIdentifier 群聊ID
     * @param groupName 群聊名称
     * @param access_token token
     * @return
     */
    public static String update(String groupIdentifier, String groupName, String access_token){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uid", groupIdentifier);
        jsonObject.put("name", groupName);
        String url = MessageFormat.format(Configs.GROUP_UPDATE_URL, access_token);
        return HttpClientUtil.post(url, jsonObject.toJSONString());
    }

    /**
     * 查询用户所在的群组
     * @param opUserName
     * @param access_token
     * @return
     */
    public static JSONArray listUserGroup(String opUserName, String access_token){
        String url = MessageFormat.format(Configs.GROUP_LISTUSERGROUP_URL, opUserName, access_token);
        return JSONArray.parseArray(HttpClientUtil.get(url));
    }

    /**
     * 查询群组内的用户
     * @param groupIdentifier
     * @param access_token
     * @return
     */
    public static JSONArray listUser(String groupIdentifier, String access_token){
        String url = MessageFormat.format(Configs.GROUP_LISTUSER_URL, groupIdentifier, access_token);
        return JSONArray.parseArray(HttpClientUtil.get(url));
    }


    public static void main(String[] args) throws Exception {
    	//System.out.println(createGroup("hewenqing,yanyuan,chencao","何文清，晏远，test","e4e5cf11-f6bd-462c-920b-6fc82c49d068"));

        //http://112.124.102.52:8080/api/group/add/{groupIdentifier}-{opAccountUid}
        System.out.println(addContact("80ffb74a-ad7d-4d7e-8230-ab85ac4f9786", "yanyuan", "chengbin,hewenqin", "58fbbe42-7cc5-401b-80a1-bc2e4c5078da"));

    }

}
