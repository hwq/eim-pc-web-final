package com.foreveross.eimpc.web.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.foreveross.eimpc.web.constants.Configs;
import com.foreveross.eimpc.web.interceptor.Token;
import com.foreveross.eimpc.web.service.ContactManager;
import com.foreveross.eimpc.web.service.LoginManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


@Controller
public class LoginController extends BaseController {

    static Map<String, JSONObject> access_token_map = new HashMap<String, JSONObject>();

    @RequestMapping(value = "login.do", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @ResponseBody
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    public Map<String, Object> login(HttpServletRequest req) {
        Long currentTime = System.currentTimeMillis();
        Map<String, Object> map = new HashMap<String, Object>();
        JSONObject user = new JSONObject();
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String result = LoginManager.doLogin(username, password, req);
        JSONObject jsonObject = JSON.parseObject(result);
        user.put("username", username);
        user.putAll(jsonObject);
        if (user.getBoolean("login")) {
            result = LoginManager.doGrant();
            jsonObject = JSON.parseObject(result);
            user.putAll(jsonObject);
            HttpSession session = req.getSession(true);
            session.setAttribute("user", user);
            map.put("result", "success");
        } else {
            map.put("error", "用户名或密码错误，请重新输入!");
        }
        System.out.println("登陆耗时 ： " + (System.currentTimeMillis() - currentTime));
        return map;
    }

    @RequestMapping(value = "main.do", method = RequestMethod.GET)
    @Token(save = true)
    public String main() {
        return "main";
    }

    @RequestMapping(value = "main.do", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> main(HttpServletRequest req) {
        Long currentTime = System.currentTimeMillis();
        Map<String, Object> returnMap = new HashMap<String, Object>();
        HttpSession session = req.getSession(true);
        JSONObject user = (JSONObject) session.getAttribute("user");
        JSONObject result = ContactManager.queryAll(user.getString("username"), user.getString("accessToken"), 0, user.getString("access_token"));
        returnMap.put("result", result);
        returnMap.put("user", user);
        returnMap.put("apiAddress", Configs.API_ADDRESS);
        returnMap.put("feedbackAddress", Configs.FEEDBACK_ADDRESS);
        System.out.println("数据初始化耗时 ： " + (System.currentTimeMillis() - currentTime));
        return returnMap;
    }

    @RequestMapping(value = "refreshToken.do", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> refreshToken(HttpServletRequest request) {
        Map<String, Object> result = new HashMap<String, Object>();
        HttpSession session = request.getSession(true);
        JSONObject user = (JSONObject) session.getAttribute("user");
        JSONObject data = LoginManager.refreshToken(user.getString("access_token"));
        user.put("access_token", data.getString("access_token"));
        user.put("expires_in", data.getString("expires_in"));
        result.put("result", data);
        return result;
    }
}
