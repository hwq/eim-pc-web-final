package com.foreveross.eimpc.web.test;

import java.util.Date;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.auth.DigestScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
/**
 * 通过ESB调用客户端测试类
 * @author 
 *
 */
public class TestCXF {

	public static String testURL(String url,String usr,String pwd) throws Exception {
		String result = "";;
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpHost targetHost = new HttpHost("10.103.124.80", 9999, "http");
		httpclient.getCredentialsProvider().setCredentials(
				new AuthScope(targetHost.getHostName(), targetHost.getPort()),
				new UsernamePasswordCredentials(usr, pwd));
		AuthCache authCache = new BasicAuthCache();
		DigestScheme digestAuth = new DigestScheme();
		digestAuth.overrideParamter("realm", "some realm");
		digestAuth.overrideParamter("nonce", "whatever");
		authCache.put(targetHost, digestAuth);
		BasicHttpContext localcontext = new BasicHttpContext();
		localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);
		HttpGet get = new HttpGet(url);
		get.addHeader("accept-encoding","gzip, deflate");
		get.addHeader("ContentType", "application/xml");
		try {
			Date startDate = new Date();
			boolean isGzipEncoding = false;
				HttpResponse response = httpclient.execute(targetHost, get,
						localcontext);
				Header[] heads =  response.getAllHeaders();
				for(Header head : heads ) {
					//System.out.println(head.getName() + ":" + head.getValue());
					if("Content-Encoding".equals(head.getName())) {
						if(head.getValue().indexOf("gzip") != -1) {
							isGzipEncoding = true;
						}
					}
				}
				if(!isGzipEncoding) {
					result= EntityUtils.toString(response.getEntity(),"UTF-8"); 
				} else {
			        HttpEntity entity = response.getEntity();  
			        Header ceheader = entity.getContentEncoding(); 
			        if (ceheader != null) {  
			            HeaderElement[] codecs = ceheader.getElements();  
			            for (int j = 0; j < codecs.length; j++) {  
			                if (codecs[0].getName().equalsIgnoreCase("gzip")) { 
			                	GzipDecompressingEntity gzip = new GzipDecompressingEntity(  
			                            response.getEntity());
			                    response.setEntity(gzip);      
			                    result=  EntityUtils.toString(gzip, "UTF-8");
			                    break;
			                }
			            }  
			        }  
				}
			Date endDate = new Date();
			System.out.println("开销时间：" + (endDate.getTime() - startDate.getTime()));
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
		return result;
	}
	  
	public static void main(String[] args) throws Exception{
		//String url = "/route/api/v1/td/fltinfo/id/59599249/FLTINFO-INCLUDE-fltDt_alnCd_fltNr_latestDepArpCd_schDepDt_schArvDt_arvArpCd_depArpCd_latestArvArpCd_latestDepDt_latestArvDt_latestTailNr_depStsCd_legStsCd_soflSeqNr";
		String url = "/esb/api/soap/v2/ope/getAirPath/?fltNr=695#4&fltDt=2013-04-06&depCd=SJW&arvCd=DLC";
		url = "/route/api/v2/weather/query/newInfo/?optArea=CYQM&optType=SP";
//		url = "/route/api/v1/his/fltsocinfo/socyxw/?startDate=20130605&endDate=20130605&code=WUH";
//		url = "/route/api/v1/his/fltsocinfo/socyxw/?startDate=20130605&endDate=20130605&code=WUH";
//		url = "/route/api/v3/ext/fltpl/base/?fltDt=20130606";
//		url = "/route/api/v2/psg/scPsgInfo/psg_typ/?flightDate=20111115&flightNo=CZ3101A";
//		url = "/wsroute/api/soap/v2/trs/getTransitFlightOverviews/?fltDt=2013-06-14&fltNo=CZ3661&trsAirport=CAN&fltType=I";
		//url = "/route/api/v1/td/fltinfo/core_kv/?latestDepDtFrom=201306160000&latestDepDtTo=201306162359&sortFilters=FLTINFO-alnCd_A";
		//url = "/route/api/v1/td/fltchg/general/?fltDt=20130618&type=I&branchCd=ALL";
		//url= "/route/api/v2/cdm/cmdDorCldInfo/cmd_typ/?fltDtFrom=20130619&fltDtTo=20130619";
		url = "/route/api/v1/td/fltchgk/flt_typ/?operTmStart=20130619093400&operTmEnd=20130620093400";
		String usr = "NPS";
		String pwd = "NPS";
		String resultMsg = testURL(url,usr,pwd);
		System.out.println(resultMsg);
		
//		ByteArrayInputStream  bs = new ByteArrayInputStream(resultMsg.getBytes());
//		JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
//		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//		Root root = (Root) jaxbUnmarshaller.unmarshal(bs);
//		System.out.println(root.getInfo().get(0).getOptTxt());
		
/*      HttpPost httpPost =  new HttpPost("/postroute/api/v1/his/flthisarpinfo/flt_typ/");
		BasicNameValuePair n1 = new BasicNameValuePair("name", "name");
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(n1);
		HttpEntity requestHttpEntity = new UrlEncodedFormEntity(list);
		httpPost.setEntity(requestHttpEntity);
*/
	}
}
