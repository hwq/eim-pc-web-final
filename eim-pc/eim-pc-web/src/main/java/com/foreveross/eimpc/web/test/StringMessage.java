package com.foreveross.eimpc.web.test;

import org.msgpack.MessagePack;
import org.msgpack.annotation.Message;

@Message
public class StringMessage {

    public String name;
    public String content;
    
	public static void main(String[] args) throws Exception {
		StringMessage src = new StringMessage();
	        src.name = "fjsdfjl";
	        src.content = "Hello, World!";

	        MessagePack msgpack = new MessagePack();
	        // Serialize
	        byte[] bytes = msgpack.write(src);
	        for(int i=0; i<bytes.length;i++){
	        	System.out.print(bytes[i]);
	        }
	        System.out.println(bytes);
	        
	        // Deserialize
	        StringMessage dst = msgpack.read(bytes, StringMessage.class);
	        System.out.println(dst.name);
	        System.out.println(dst.content);

	}

}
