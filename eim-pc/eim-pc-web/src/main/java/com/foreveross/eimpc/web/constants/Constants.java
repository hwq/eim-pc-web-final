package com.foreveross.eimpc.web.constants;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Constants implements Serializable {

	/**
	 * @Fields serialVersionUID : 
	 */
	private static final long serialVersionUID = 8621584716119390337L;
	private static final Logger logger = LoggerFactory.getLogger(Constants.class);
	public static final String SUCCESS = "SUCCESS";
	public static final String FAIL = "FAIL";
	
	public static final String LOGIN_ERROR = "004";

	public static final String API_Bad_Request = "001";
	public static final String API_Validate = "002";
	public static final String API_EXTERNAL = "003";
	public static final String API_Internal = "500";
	
	/**
	 * 群聊状态: 1正常
	 */
	public static final int GROUP_CHAT_STATUS_NORMAL = 1;
	/**
	 * 群聊状态: 0关闭
	 */
	public static final int GROUP_CHAT_STATUS_CLOSED = 0;

	/**
	 * 新消息通知;1通知,0不通知
	 */
	public static final int GROUP_CHAT_MSG_NOTITY = 1;
	/**
	 * 新消息通知;1通知,0不通知
	 */
	public static final int GROUP_CHAT_MSG_NOTITY_WITHOUT = 0;
	
	/**
	 * 保存到通讯录;1保存,0不保存
	 */
	public static final int GROUP_CHAT_CONTACTS_SAVE = 1;
	/**
	 * 保存到通讯录;1保存,0不保存
	 */
	public static final int GROUP_CHAT_CONTACTS_SAVE_WITHOUT = 0;
	
	/**
	 * 置顶聊天;1置顶;0不置顶
	 */
	public static final int GROUP_CHAT_TOP = 1;
	/**
	 * 置顶聊天;1置顶;0不置顶
	 */
	public static final int GROUP_CHAT_TOP_WITHOUT = 0;
	
	/**
	 * 显示群成员的昵称;1显示;0不显示
	 */
	public static final int GROUP_CHAT_SHOW_NICENAME = 1;
	/**
	 * 显示群成员的昵称;1显示;0不显示
	 */
	public static final int GROUP_CHAT_SHOW_NICENAME_WITHOUT = 0;
}
