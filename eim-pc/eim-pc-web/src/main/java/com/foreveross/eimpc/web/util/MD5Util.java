package com.foreveross.eimpc.web.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by lingen on 14-4-28.
 */
public class MD5Util {

    private static final String MD5 = "MD5";

    private static final String SHA = "SHA-256";


    /**
     * encrypt value
     *
     * @param password
     * @return
     */
    public static String encode(final String password) {
        try {
            final MessageDigest md = MessageDigest.getInstance(MD5);
            MessageDigest sha256 = MessageDigest.getInstance(SHA);
            byte[] passBytes = md.digest((password).getBytes());
            byte[] passHash = sha256.digest(hex(passBytes).getBytes());
            return hex(passHash);
        } catch (final NoSuchAlgorithmException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private static String hex(final byte[] bytes) {
        final StringBuffer buffer = new StringBuffer(bytes.length * 2);
        for (final byte element : bytes) {
            if ((element & 0xff) < 0x10) {
                buffer.append("0");
            }
            buffer.append(Long.toString(element & 0xff, 16));
        }
        return buffer.toString();
}

    public static void main(String args[]){
     System.out.println(MD5Util.encode("gu.yd"));
    }
}
