package com.foreveross.eimpc.web.exception;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 3293182144262286732L;

	private String errcode;
	private String errmsg;

	public ApiException() {
		super();
	}

	public ApiException(String code, String msg) {
		super();
		this.errcode = code;
		this.errmsg = msg;
	}
	
	public ApiException(int code, String msg) {
		super();
		this.errcode = code+"";
		this.errmsg = msg;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return "errcode:"+errcode+",errmsg:"+errmsg;
	}

}
