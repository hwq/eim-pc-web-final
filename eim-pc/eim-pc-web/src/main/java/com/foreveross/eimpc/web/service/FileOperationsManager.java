package com.foreveross.eimpc.web.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.foreveross.eimpc.web.constants.Configs;
import com.foreveross.eimpc.web.util.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by yanyuan on 2014/11/24.
 */
public class FileOperationsManager {

    public static Logger logger = LoggerFactory.getLogger(FileOperationsManager.class);

    /**
     * 文件上传
     * @param suffix
     * @param fileBytes
     * @param access_token
     * @return
     */
    public static String uploadStreamMd5(String suffix, byte[] fileBytes, String access_token){
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("suffix", suffix);
        postData.put("file", fileBytes);
        postData.put("fileMd5", UUID.randomUUID().toString());
        logger.info(Configs.UPLOAD_USER_AVATAR);
        return HttpClientUtil.postFile(MessageFormat.format(Configs.UPLOAD_FILE, access_token), postData, null, null, null, null, null, null);

    }
    
    public static String uploadImg(String suffix, byte[] fileBytes, String access_token){
    	logger.info(Configs.UPLOAD_USER_AVATAR);
        String url=MessageFormat.format(Configs.UPLOAD_IMG, access_token)+"&suffix="+suffix;
        return  HttpClientUtil.post(url, fileBytes, null, null, null);
   }


    /**
     * 文件上传
     * @param fileName
     * @param suffix
     * @param fileBytes
     * @param access_token
     * @return
     */
    public static String uploadStreamMd5(String fileName, String suffix, byte[] fileBytes, String access_token){
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("fileName", fileName);
        postData.put("suffix", suffix);
        postData.put("file", fileBytes);
        postData.put("fileMd5", UUID.randomUUID().toString());
        logger.info(Configs.UPLOAD_FILE);
        return HttpClientUtil.postFile(MessageFormat.format(Configs.UPLOAD_FILE, access_token), postData, null, null, null, null, null, null);
    };

    public static void main(String[] args){
        String str = "{'TFS_FILE_NAME':'54746388e4b0f8f18b4e2ed2'}";
        JSONObject jsonObject = JSON.parseObject(str);
        System.out.println(jsonObject.getString("TFS_FILE_NAME"));

        String str1 = "data:image/ jpeg;base64,/9j/4AAQSkZJRg";

        String str2 = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,UEsDBBQABg";

        System.out.println(str1.substring(str1.indexOf("base64,") + 7));

        System.out.println(str2.replaceAll("data:\\s*;base64:", ""));


    }
}
