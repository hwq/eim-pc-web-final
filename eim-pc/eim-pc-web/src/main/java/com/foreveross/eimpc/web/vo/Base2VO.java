package com.foreveross.eimpc.web.vo;
/**
 * 
 * @author foss
 * @description 这个类是组织和部门的父类，继承了基类{@link BaseVO.java}
 */

public class Base2VO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5677369965495126584L;
	private int company_id;
	private int identifier;		//下级标示
	private int memberCount;
	private int numberOfStaffs;
	private int superior;
	private String type;
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public int getIdentifier() {
		return identifier;
	}
	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}
	public int getMemberCount() {
		return memberCount;
	}
	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}
	public int getNumberOfStaffs() {
		return numberOfStaffs;
	}
	public void setNumberOfStaffs(int numberOfStaffs) {
		this.numberOfStaffs = numberOfStaffs;
	}
	public int getSuperior() {
		return superior;
	}
	public void setSuperior(int superior) {
		this.superior = superior;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
