package com.foreveross.eimpc.web.vo;
/**
 * 
 * @author foss
 * @description 人员类
 */
public class UserVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3664918221707066570L;
	private String avatar;
	private String backgroudImg;
	private int department;
	private String email;
	private String firstNameLetter;
	private String fullPinYin;
	private short gender;
	private String mobile;
	private String post;
	private String uid;
	private String username;
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getBackgroudImg() {
		return backgroudImg;
	}
	public void setBackgroudImg(String backgroudImg) {
		this.backgroudImg = backgroudImg;
	}
	public int getDepartment() {
		return department;
	}
	public void setDepartment(int department) {
		this.department = department;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstNameLetter() {
		return firstNameLetter;
	}
	public void setFirstNameLetter(String firstNameLetter) {
		this.firstNameLetter = firstNameLetter;
	}
	public String getFullPinYin() {
		return fullPinYin;
	}
	public void setFullPinYin(String fullPinYin) {
		this.fullPinYin = fullPinYin;
	}
	public short getGender() {
		return gender;
	}
	public void setGender(short gender) {
		this.gender = gender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
