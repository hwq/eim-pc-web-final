package com.foreveross.eimpc.web.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.foreveross.eimpc.web.constants.Configs;
import com.foreveross.eimpc.web.util.HttpClientUtil;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

public class ContactManager {

    public static org.slf4j.Logger logger = LoggerFactory.getLogger(ContactManager.class);

	public static JSONObject queryUser(String username, String access_token){
		String url = MessageFormat.format(Configs.QUERY_USER_URL, username, access_token);
		String result = HttpClientUtil.get(url);
		JSONArray user = JSON.parseArray(result);
		return user.getJSONObject(0);
	}

	//目前这个方法之支持2级目录，因为数据结构不一样，后续再改
	public static JSONObject queryAll(String userName, String accessToken, long lastModifyDate, String access_token){
		
		String url = MessageFormat.format(Configs.QUERY_ALL_URL, accessToken, lastModifyDate, access_token);
		String result = HttpClientUtil.get(url);
		System.out.println(result);
		return JSON.parseObject(result);
	}
	
	/*public static byte[] queryAvatar(String userName, String access_token){
		String url = MessageFormat.format(Configs.QUERY_USER_AVATAR, userName, access_token);
		byte[] image = HttpClientUtil.getByte(url, getData, headerData)
		return null;
	}*/

    public static String uploadUserAvatar(String username, File avatar, String access_token) {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("username", username);
        postData.put("avatar", avatar);
        logger.info(Configs.UPLOAD_USER_AVATAR);
        return HttpClientUtil.postFile(MessageFormat.format(Configs.UPLOAD_USER_AVATAR, access_token), postData, null, null, null, null, null, null);
    }

    public static String uploadUserAvatar(String username, byte[] avatar, String access_token) {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("username", username);
        postData.put("avatar", avatar);
        logger.info(Configs.UPLOAD_USER_AVATAR);
        return HttpClientUtil.postFile(MessageFormat.format(Configs.UPLOAD_USER_AVATAR, access_token), postData, null, null, null, null, null, null);
    }

    /**
     * 修改星标联系人
     * @param username
     * @param operation
     * @param accessToken
     * @return
     */
    public static String commonlyUsedContacter(String username, String operation, String accessToken, String access_token){
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("action", operation);
        jsonObject.put("lastModifyDate", new Date());
        jsonObject.put("username", username);
        jsonArray.add(jsonObject);
        String url = MessageFormat.format(Configs.COMMONLY_USED_CONTACTER, accessToken, access_token);
        return HttpClientUtil.post(url, jsonArray.toJSONString());
    }

    public static JSONObject queryCommonlyUsedContacter(String accessToken, String access_token){
        String url = MessageFormat.format(Configs.QUERY_COMMONLY_USED_CONTACTER, accessToken, 0, access_token);
        return JSON.parseObject(HttpClientUtil.get(url));
    }

    public static JSONObject queryChangeInfo(String msgId, String notifyType, String action, String access_token,List<String> ids){
        String url = MessageFormat.format(Configs.QUERY_CHANGE_INFO, msgId, notifyType, action, access_token);
        JSONArray arr = new JSONArray();
        for(String id : ids){
            arr.add(id);
        }
        return JSON.parseObject(HttpClientUtil.post(url, arr.toString()));
    }


    /**
     * 以字节为单位读取文件，常用于读二进制文件，如图片、声音、影像等文件。
     */
    public static byte[] readFileByBytes(File file) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
        InputStream in = null;
        try {
            // 一次读多个字节
            byte[] tempbytes = new byte[100];
            int byteread = 0;
            in = new FileInputStream(file);
            // 读入多个字节到字节数组中，byteread为一次读入的字节数
            while ((byteread = in.read(tempbytes)) != -1) {
                bos.write(tempbytes, 0, byteread);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                    bos.close();
                } catch (IOException e1) {
                }
            }
        }
        return bos.toByteArray();
    }


    public static void main(String[] args) throws Exception {
        //	queryAll("6E7EC6DD82EE4D06B42F53DFBD91BF31",0,"51728f39-31ee-4206-a65d-b0c0c136cd5a");
        File file = new File("C:\\Users\\peguinDance\\Pictures\\png\\1.jpg");
       // String token = "51a9b041-971e-44b0-b24a-fdafa979a154";
//        try {
//            String url = "http://112.124.65.217:8080/api/service/contact/avatar/upload";
////            url = "http://172.16.1.44:8080/api/service/contact/avatar/upload";
//            //发送
//            Map<String, Object> postData = new HashMap<String, Object>();
//            postData.put("username", "yanyuan");
//            postData.put("avatar", readFileByBytes(file));
//           // postData.put("access_token", token);
//
//            HttpClientUtil.postFile(url, postData, null, null, null, null, null, null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

}
