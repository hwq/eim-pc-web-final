package com.foreveross.eimpc.web.util;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.ProgressListener;

import com.foreveross.eimpc.web.vo.ProgressVO;

public class FileUploadListener implements ProgressListener {
	private HttpSession session;
	private String sfile = "";
	
	 public FileUploadListener(HttpSession _session,String file) {  
		    this.sfile = file;
	        session=_session;  
	        ProgressVO ps = new ProgressVO();  
	        session.setAttribute("upload_ps"+sfile, ps);  
	    }  
	
	public void update(long bytesRead, long contentLength, int items) {
		ProgressVO ps = (ProgressVO) session.getAttribute("upload_ps"+sfile);
		ps.setpBytesRead(bytesRead);
		ps.setpContentLength(contentLength);
		ps.setpItems(items);
		// 更新
		session.setAttribute("upload_ps"+sfile, ps);
	}

	public String getSfile() {
		return sfile;
	}

	public void setSfile(String sfile) {
		this.sfile = sfile;
	}

}