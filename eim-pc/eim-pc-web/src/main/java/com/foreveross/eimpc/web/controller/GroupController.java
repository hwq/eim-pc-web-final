package com.foreveross.eimpc.web.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.foreveross.eimpc.web.service.GroupManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/group")
public class GroupController extends BaseController{

    @ResponseBody
    @RequestMapping(value = "create.do", method = RequestMethod.POST)
    public JSONObject createGroup(HttpServletRequest req){
        String usernames = req.getParameter("usernames");
        String groupName = req.getParameter("name");
        HttpSession session = req.getSession(true);
		JSONObject user = (JSONObject)session.getAttribute("user");
        String creator = user.getString("username");
		String result  = GroupManager.createGroup(usernames, groupName, creator, user.getString("access_token"));
        return JSON.parseObject(result);
    }
    
    
    @ResponseBody
    @RequestMapping(value = "query.do", method = RequestMethod.POST)
    public JSONObject queryGroup(HttpServletRequest req){
        String groupUid = req.getParameter("groupUid");
        HttpSession session = req.getSession(true);
		JSONObject user = (JSONObject)session.getAttribute("user");
		String result  = GroupManager.queryGroup(groupUid, user.getString("access_token"));
        return JSON.parseObject(result);
    }

    @ResponseBody
    @RequestMapping(value = "addContact.do", method = RequestMethod.POST)
    public void addContact(HttpServletRequest request){
        String addContactNames = request.getParameter("addContactNames");
        String groupIdentifier = request.getParameter("groupIdentifier"); //群组ID
        HttpSession session = request.getSession(true);
        JSONObject user = (JSONObject)session.getAttribute("user");
        String opUserName = user.getString("username");
        String access_token = user.getString("access_token");
        GroupManager.addContact(groupIdentifier, opUserName, addContactNames, access_token);
    }

    @ResponseBody
    @RequestMapping(value = "exit.do", method = RequestMethod.POST)
    public void exit(HttpServletRequest request){
        String exitContactName = request.getParameter("exitContactName");
        String groupIdentifier = request.getParameter("groupIdentifier"); //群组ID
        HttpSession session = request.getSession(true);
        JSONObject user = (JSONObject)session.getAttribute("user");
        String opUserName = user.getString("username");
        String access_token = user.getString("access_token");
        GroupManager.exit(groupIdentifier, opUserName, exitContactName, access_token);
    }

    @ResponseBody
    @RequestMapping(value = "update.do", method = RequestMethod.POST)
    public void update(HttpServletRequest request){
        String groupIdentifier = request.getParameter("groupIdentifier"); //群组ID
        String groupName = request.getParameter("groupName");
        HttpSession session = request.getSession(true);
        JSONObject user = (JSONObject)session.getAttribute("user");
        String access_token = user.getString("access_token");
        GroupManager.update(groupIdentifier, groupName, access_token);

    }

    @ResponseBody
    @RequestMapping(value = "listUserGroup.do", method = RequestMethod.GET)
    public Map<String, Object> listUserGroup(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        HttpSession session = request.getSession(true);
        JSONObject user = (JSONObject)session.getAttribute("user");
        String opUserName = user.getString("username");
        String access_token = user.getString("access_token");
        result.put("result", GroupManager.listUserGroup(opUserName, access_token));
        return result;
    }

    @ResponseBody
      @RequestMapping(value = "listUser.do", method = RequestMethod.POST)
      public Map<String, Object> listUser(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        HttpSession session = request.getSession(true);
        String groupIdentifier = request.getParameter("groupIdentifier");
        JSONObject user = (JSONObject)session.getAttribute("user");
        String access_token = user.getString("access_token");
        result.put("result", GroupManager.listUser(groupIdentifier, access_token));
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "userGroups.do", method = RequestMethod.POST)
    public Map<String, Object> userGroups(HttpServletRequest request){
        Map<String, Object> result = new HashMap<String, Object>();
        HttpSession session = request.getSession(true);
        JSONObject user = (JSONObject)session.getAttribute("user");
        JSONArray userGroups = GroupManager.listUserGroup(user.getString("username"), user.getString("access_token"));
        result.put("userGroups", userGroups);
        return result;
    }
}
