package com.foreveross.eimpc.web.vo;

import java.io.Serializable;
/**
 * 
 * @author foss
 * @description 这个类是组织、部门和联系人的基类
 */
public class BaseVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7530096173454556768L;
	private String name;
	private String firstRoot;	//顶级组织标示
	private String pinyin;		//名称拼音组合
	private short status;		//当前状态
	private int sort;			//排序
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstRoot() {
		return firstRoot;
	}
	public void setFirstRoot(String firstRoot) {
		this.firstRoot = firstRoot;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public short getStatus() {
		return status;
	}
	public void setStatus(short status) {
		this.status = status;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	
}
