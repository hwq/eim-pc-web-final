package com.foreveross.eimpc.web.vo;

/**
 * 
 * @author foss
 * @description 部门类
 */
public class Department extends Base2VO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3628368639166689868L;
	
	private String groupUid;

	public String getGroupUid() {
		return groupUid;
	}

	public void setGroupUid(String groupUid) {
		this.groupUid = groupUid;
	}
	
}
