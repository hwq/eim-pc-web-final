package com.foreveross.eimpc.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.foreveross.eimpc.web.service.ContactManager;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/contact")
public class ContactController extends BaseController{

    @ResponseBody
    @RequestMapping(value = "uploadUserAvatar.do", method = RequestMethod.POST)
    public Map<String, Object> uploadUserAvatar(HttpServletRequest req) throws IOException{
        Map<String, Object> returnMap = new HashMap<String, Object>();
        String avatar = req.getParameter("avatar");
        byte[] avatarBytes = new Base64().decode(base64StringFormat(avatar));
        HttpSession session = req.getSession(true);
        JSONObject user = (JSONObject) session.getAttribute("user");
        String result = ContactManager.uploadUserAvatar(user.getString("username"), avatarBytes, user.getString("access_token"));
        logger.info(result);
        returnMap.put("result", "success");
        return returnMap;
    }

    @ResponseBody
    @RequestMapping(value = "avatar.do", method = RequestMethod.GET)
    public String avatar(HttpServletRequest req, HttpServletResponse res) {
        String avatar = req.getParameter("avatar");
        String name = req.getParameter("name");

        byte[] b = Base64.decodeBase64(avatar);
        return "{result:true}";
    }


    @ResponseBody
    @RequestMapping(value = "commonlyUsedContacter.do", method = RequestMethod.POST)
    public Map<String, Object> commonlyUsedContacter(HttpServletRequest req){
        Map<String, Object> returnMap = new HashMap<String, Object>();
        String operation = req.getParameter("operation");
        String username = req.getParameter("username");
        HttpSession session = req.getSession(true);
        JSONObject user = (JSONObject) session.getAttribute("user");
        ContactManager.commonlyUsedContacter(username, operation, user.getString("accessToken"), user.getString("access_token"));
        returnMap.put("result", "success");
        return returnMap;
    }


    @ResponseBody
    @RequestMapping(value = "queryCommonlyUsedContacter.do", method = RequestMethod.POST)
    public Map<String, Object> queryCommonlyUsedContacter(HttpServletRequest req){
        Map<String, Object> returnMap = new HashMap<String, Object>();
        HttpSession session = req.getSession(true);
        JSONObject user = (JSONObject) session.getAttribute("user");
        JSONObject result = ContactManager.queryCommonlyUsedContacter(user.getString("accessToken"), user.getString("access_token"));
        returnMap.put("result", result);
        return returnMap;
    }

    @ResponseBody
    @RequestMapping(value = "queryChangeInfo.do", method = RequestMethod.POST)
    public Map<String, Object> queryChangeInfo(HttpServletRequest req){
        Map<String, Object> returnMap = new HashMap<String, Object>();
        HttpSession session = req.getSession(true);
        String msgId = req.getParameter("msgId");
        String notifyType = req.getParameter("notifyType");
        String action = req.getParameter("action");
        String idsStr = req.getParameter("ids");
        JSONObject user = (JSONObject) session.getAttribute("user");
        List<String> ids = new ArrayList<String>();
        for(String id : idsStr.split(",")){
            ids.add(id);
        }
        JSONObject result = ContactManager.queryChangeInfo(msgId, notifyType, action, user.getString("access_token"), ids);
        returnMap.put("result", result);
        return returnMap;
    }
}
