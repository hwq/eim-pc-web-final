package com.foreveross.eimpc.web.constants;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.MessageFormat;

public class Configs implements Serializable {

	/**
	 * @Fields serialVersionUID : 
	 */
	private static final long serialVersionUID = 8621584716119390337L;
	private static final Logger logger = LoggerFactory.getLogger(Configs.class);
	private static final String LOCAL_FILE = "config.properties";
	
	public static String API_ADDRESS = "http://112.124.65.217:8080";
	public static String AUTH_ADDRESS = "http://112.124.65.217:8080";
    public static String FEEDBACK_ADDRESS = "http://112.124.102.52:8003";
	public static String LOGIN_URL = "/auth/v1/auth";
	public static String GRANT_URL = "/api/oauth/token?grant_type={0}&client_id={1}&client_secret={2}";
    public static String REFRESH_TOKEN = "/api/oauth/token?grant_type={0}&client_id={1}&client_secret={2}&refresh_token={3}";
    /**
     * contact service
     */
	public static String QUERY_ALL_URL="/api/service/contact/all?accessToken={0}&lastModifyDate={1}&access_token={2}";
	public static String QUERY_USER_URL="/api/service/contact/get?usernames={0}&access_token={1}";
	public static String QUERY_USER_AVATAR="/api/service/contact/avatar/download?username={0}&access_token={1}";
	public static String UPLOAD_USER_AVATAR="/api/service/contact/avatar/upload";
    public static String COMMONLY_USED_CONTACTER = "/api/service/contact/commonlyUsedContacter/create?accessToken={0}&access_token={1}";
    public static String QUERY_COMMONLY_USED_CONTACTER = "/api/service/contact/commonlyUsedContacter/get?accessToken={0}&lastModifyDate={1}&access_token={2}";

    /**
     * group service
     */
    public static String GROUP_CREATE_URL="/api/service/group/create";
	public static String GROUP_QUERY_URL="/api/service/group/getDetailGroup/{0}?access_token={1}";
    public static String GROUP_ADD_CONTACT_URL = "/api/service/group/add/{0}-{1}?access_token={2}";
    public static String GROUP_EXIT_URL = "/api/service/group/exit/{0}-{1}-{2}?access_token={3}";
    public static String GROUP_UPDATE_URL = "api/service/group/update?access_token={0}";
    public static String GROUP_LISTUSERGROUP_URL = "/api/service/group/listUserGroup/{0}?access_token={1}";
    public static String GROUP_LISTUSER_URL = "/api/service/group/listUser/{0}?access_token={1}";

    public static String QUERY_CHANGE_INFO = "/api/service/change/changeInfo/{0}-{1}-{2}?access_token={3}";


    public static String UPLOAD_FILE = "/api/service/file/uploadStreamMd5?access_token={0}";
    public static String UPLOAD_IMG = "/api/service/file/upload?access_token={0}";

	public static int CONNECT_TIMEOUT = 2*1000;
	public static int SOCKET_TIMEOUT = 2*1000;
	
	
	static {
		try {
			Configuration config = new PropertiesConfiguration(LOCAL_FILE);
			
			API_ADDRESS = config.getString("api_address",API_ADDRESS);
			
			AUTH_ADDRESS = config.getString("auth_address",AUTH_ADDRESS);

            FEEDBACK_ADDRESS = config.getString("feedback_address", FEEDBACK_ADDRESS);
			
			LOGIN_URL = AUTH_ADDRESS.concat(config.getString("login_url", LOGIN_URL));
			
			GRANT_URL = API_ADDRESS.concat(config.getString("grant_url", GRANT_URL));

            REFRESH_TOKEN = API_ADDRESS.concat(config.getString("refresh_token", REFRESH_TOKEN));

			QUERY_ALL_URL = API_ADDRESS.concat(config.getString("query_all_url", QUERY_ALL_URL));
			
			QUERY_USER_URL = API_ADDRESS.concat(config.getString("query_all_url", QUERY_USER_URL));
			
			QUERY_USER_AVATAR = API_ADDRESS.concat(config.getString("query_user_avatar", QUERY_USER_AVATAR));

            UPLOAD_USER_AVATAR = API_ADDRESS.concat(config.getString("upload_user_avatar", UPLOAD_USER_AVATAR));

            COMMONLY_USED_CONTACTER = API_ADDRESS.concat(config.getString("commonly_used_contacter", COMMONLY_USED_CONTACTER));

            QUERY_COMMONLY_USED_CONTACTER = API_ADDRESS.concat(config.getString("query_commonly_used_contacter", QUERY_COMMONLY_USED_CONTACTER));
            
            GROUP_CREATE_URL = API_ADDRESS.concat(config.getString("group_create_url", GROUP_CREATE_URL));
            
            GROUP_QUERY_URL = API_ADDRESS.concat(config.getString("group_query_url", GROUP_QUERY_URL));

            GROUP_ADD_CONTACT_URL = API_ADDRESS.concat(config.getString("group_add_contact_url", GROUP_ADD_CONTACT_URL));

            GROUP_EXIT_URL = API_ADDRESS.concat(config.getString("group_exit_url", GROUP_EXIT_URL));

            GROUP_UPDATE_URL = API_ADDRESS.concat(config.getString("group_update_url", GROUP_QUERY_URL));

            GROUP_LISTUSERGROUP_URL = API_ADDRESS.concat(config.getString("group_listUserGroup_url", GROUP_LISTUSERGROUP_URL));

            GROUP_LISTUSER_URL = API_ADDRESS.concat(config.getString("group_listUser_url", GROUP_LISTUSER_URL));

            QUERY_CHANGE_INFO = API_ADDRESS.concat(config.getString("query_change_info", QUERY_CHANGE_INFO));

            UPLOAD_FILE = API_ADDRESS.concat(config.getString("upload_file", UPLOAD_FILE));
            
            UPLOAD_IMG = API_ADDRESS.concat(config.getString("upload_img", UPLOAD_IMG));
            GRANT_URL = MessageFormat.format(GRANT_URL, config.getString("grant_type"), config.getString("grant_id"), config.getString("grant_secret"));
			
			CONNECT_TIMEOUT = config.getInt("connect_timeout", CONNECT_TIMEOUT);
			SOCKET_TIMEOUT = config.getInt("socket_timeout", SOCKET_TIMEOUT);
		} catch (Exception e) {
			logger.info("initial properties file is {}", LOCAL_FILE);
			logger.error("initial properties error!! using default configuration!!", e);
		}
	}
	
}
