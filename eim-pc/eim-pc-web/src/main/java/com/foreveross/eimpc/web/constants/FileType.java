package com.foreveross.eimpc.web.constants;

/**
 * Created by yanyuan on 2014/11/26.
 */
public enum FileType {

    IMG("图片"),FILE("文件");

    private String label;

    FileType(String label){
        this.label = label;
    }

    public static final String imgs = "bmp,jpg,jpeg,tiff,gif,pcx,tga,exif,fpx,svg,psd,cdr,pcd,dxf,ufo,eps,ai,raw,png";

    public static FileType getFileType(String suffix){
        if(imgs.contains(suffix)){
            return  IMG;
        }else {
            return FILE;
        }
    }
}
