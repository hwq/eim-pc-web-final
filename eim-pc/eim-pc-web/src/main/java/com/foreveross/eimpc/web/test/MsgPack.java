package com.foreveross.eimpc.web.test;

import java.util.Map;

import org.msgpack.MessagePack;
import org.msgpack.template.Templates;
import org.msgpack.type.Value;


public class MsgPack {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		int[] ints = new int[]{130, 164, 110, 97, 109, 101, 167, 102, 106, 115, 100, 102, 106, 108, 167, 99, 111, 110, 116, 101, 110, 116, 173, 72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33};
		byte[] byts  =new byte[ints.length]; 
		for(int j=0;j<byts.length;j++){
			byts[j] = (byte)ints[j];
		}
		
		   MessagePack msgpack = new MessagePack();
			Map<String, Value> vMap;
			vMap = msgpack.read(byts,Templates.tMap(Templates.TString, Templates.TValue));
			System.out.println(vMap.toString());
	}

}
