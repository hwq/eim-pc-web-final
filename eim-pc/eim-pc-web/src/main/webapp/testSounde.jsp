<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<script language="javascript" type="text/javascript">
	function msgPlay() {
		var myAuto = document.getElementById('msgaudio');
		console.info("myAuto="+myAuto);
		myAuto.play();
	}
</script>
</head>
<body>
	<audio id="msgaudio"  controls="controls"  hidden="true">
            <source src="${pageContext.request.contextPath}/sound/msg.mp3" type="audio/ogg">
            <source src="${pageContext.request.contextPath}/sound/msg.wav" type="audio/mpeg">
                                您的浏览器不支持 audio 元素。
	</audio>
	<input type="button" onclick="msgPlay()" value="播放" />

</body>
</html>