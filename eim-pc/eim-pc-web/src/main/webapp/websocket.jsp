<html>
<head><title>Web Socket Test</title></head>

<%-- <%@ include file="/pages/common/header.jsp"%> --%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/other/msgpack.js"></script>

<body>

<form onsubmit="return false;">
<input type="text" name="message" value="Hello, World!"/><input type="button" value="Send Web Socket Data"
       onclick="send(this.form.message.value)" />
<h3>Output</h3>
<textarea id="responseText" style="width:500px;height:300px;"></textarea>

<p>Upload File: <input type="file" name="file" id="selectFile" onchange="sendImg(this);"/></p> 

 <img id="imgArea" src="e:/h.jpg"  height="30" width="50">
</form>
</body>
</html>
<script type="text/javascript">
var socket;
if (!window.WebSocket) {
  window.WebSocket = window.MozWebSocket;
}
if (window.WebSocket) {
  socket = new WebSocket("ws://127.0.0.1:8280/websocket");
  socket.onmessage = function(event) {
    var ta = document.getElementById('responseText');
    console.log("onmessage..."+event.data);
	var  myObject = msgpack.unpack(event.data);
	console.log(myObject.name);
    ta.value = ta.value + '\n' + myObject.content

    document.getElementById("imgArea").src = myObject.content;
  };
  socket.onopen = function(event) {
    var ta = document.getElementById('responseText');
    ta.value = "Web Socket opened!";
  };
  socket.onclose = function(event) {
    var ta = document.getElementById('responseText');
    ta.value = ta.value + "Web Socket closed"; 
  };
} else {
  alert("Your browser does not support Web Socket.");
}

function send(data) {
  if (!window.WebSocket) { return; }
  if (socket.readyState == WebSocket.OPEN) {
	var message = {
		'name' : "fjsdfjl",
		'content' : data
	};
	console.log(message);
	//message = packMessage(message, 1);
	message = packMessagenoLength(message);
	//var  myObject = msgpack.unpack(message);
	//console.log(myObject.name);
	console.log(message);
     socket.send(message);
  } else {
    alert("The socket is not open.");
  }
}

function sendImg(obj){
	if (obj.files.length != 0 ) {
		var file = obj.files[0];
		if(!((/image/i).test(file.type))){
			alert("请选择图片！！！");
		};
		var reader = new FileReader();
		if (!reader) {
			alert("浏览器不支持FileReader！！！");
		};
		reader.onloadend = function(e) {
			//读取成功，显示到页面并发送到服务器
			var imgDate = e.target.result;
			//document.getElementById("imgArea").src = e.target.result;
			console.log(imgDate);
			send(imgDate);
		};
	//	reader.readAsBinaryString(file);
		reader.readAsDataURL(file);
		//reader.readAsArrayBuffer(file);
	}
}

function packMessage(message, type){
    var content=[];
    var data = msgpack.pack(message);
    var length = data.length;
    var num = length + 4 + 1;
    var buffer = new ArrayBuffer(num);
    //有连续的空间
    if(buffer.byteLength === num){
        console.log("成功！！！");
        var dv = new DataView(buffer);
        dv.setInt32(0, length);
        dv.setInt8(4, type);
        var cBuffer = new Int8Array(buffer,5, length);
        cBuffer.set(data);
        for(var i=0;i<buffer.byteLength;i++){
        	content[i] = dv.getInt8(i);
        }
        return content;
    }else{
        //没有连续的空间
        console.log("不成功！！！");
    }
}


function packMessagenoLength(message, type){
    var content=[];
    var data = msgpack.pack(message);
    return data;
}
</script>