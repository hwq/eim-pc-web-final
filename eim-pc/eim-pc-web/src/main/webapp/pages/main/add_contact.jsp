<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="addParticipants" ng-show="showAddParticipantsBox">
    <div class="hd">
    	<h4 class="fl">发起聊天</h4>
	    <!-- <h4 class="fl">添加参与人</h4> -->
	    <span class="closeinfo" ng-click="closeAddContactBox()"></span>
	    <div class="cl"></div>
	</div>
	<div class="inputContent">
		<input type="text" class="left" ng-model="groupCondition" placeholder="搜索" ng-change="groupSearch()"/>
        <span class="search left"></span>
        <span class="searchClean left" style="right: 20px" ng-show="groupCondition.length>0 " ng-click="groupCondition='' "></span>
	</div>
	<div class="addContactListBox" ng-if="showAddParticipantsBox">
        <div>              
            <p class="offsetTopHelp"></p>
            <p class="searchPersonTitle">联系人</p>
        </div>
        <!-- organization tree -->
        <div ng-if = "groupCondition == null || groupCondition.length == 0 " ng-class="{'moveUp':groupPersons.length>0}" ng-init="showDepts=false">
            <div add-contact-org-tree></div>
            <ng-include src="'${pageContext.request.contextPath}/pages/main/template/addContactStarFriends.jsp'"></ng-include>
        </div>

        <!-- search result -->
        <div class="groupTitleDetails" ng-class="{'moveUp':groupPersons.length>0}"  ng-if="groupCondition != null && groupCondition.length > 0 ">
            <a class="friendDetail" ng-click="addGroupPerson(contact.identifier,$event);"
            	ng-repeat="contact in searchGroupAddressResult  | orderBy:['sort', 'pinyin'] | getContactStatus : groupContactIderfiers : groupPersons">
                <span ng-class="{'checked' : contact.isChecked, 'check' : !contact.isChecked}"  id="span_search_{{contact.identifier}}"></span>
                <span class="avatar" style="float: left;">
				    <img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
					<img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
				</span>
                <div class="left name" >
                    <div class="searchResult" style="overflow:hidden;text-overflow: ellipsis;">{{contact.name}}</div>
                </div>
                <p class="mystar" style="display:none"></p>
                <div class="clr"></div>
            </a>
        </div>
    </div>
	<div class="numadd" ng-show="groupPersons.length > 0" ng-mouseover="showDelete = true" ng-mouseleave="showDelete = false">
        <div class="lbtn"></div>
        <div class="addcontactimg">
            <ul id="adimg">
                <li ng-repeat="contact in groupPersons | toContacts">
                	<img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}" title="{{contact.name}}">
					<img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png" title="{{contact.name}}">
                    <span class="delete" ng-show="showDelete" ng-click="delGroupPerson(contact.identifier)"></span>
                </li>
            </ul>
        </div>
        <div class="rbtn"></div>
    </div>
	<div class="ft">
        <input ng-if="groupPersons.length==0" type="button" value="确定" ng-click="closeAddContactBox()" >
        <input ng-if="groupPersons.length>0" type="button" value="确定({{groupPersons.length}})" ng-click="createGroup();">
    </div>
</div>