<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
function doProgressLoop(prog, max, counter,id,s_filename) {
	var x ;
	var y ;
	var tid ;
	tid = window.setInterval(function(){  
		getProgress(id,s_filename); 
		try{
		document.getElementById(id+'progressBarBoxContent').style.width = parseInt(prog)+ '%';
		x = document.getElementById(id+'progress-content').innerHTML;
		y = parseInt(x);
		if (!isNaN(y)) {
			prog = y;
		}
		counter = counter + 1;
//		console.info("prog="+prog);
		document.getElementById(id+'progressBarBoxContent').style.width = parseInt(prog)+ '%';
		}catch(e){}
			if (prog > 95 || counter == 1000) { 
				window.clearInterval(tid);
			}
		},1000);   
//		console.info("tid="+tid);
    return tid;
}

function showProgress(id ,s_filename ) {
	var max = 100;
	var prog = 0;
	var counter = 0;
	console.log(id+'progressBar');
	/* document.getElementById(id+'progressBar').style.display = 'block'; */
	getProgress(id ,s_filename );
	var progressInterval = doProgressLoop(prog, max, counter,id,s_filename );
    return progressInterval;
}

function getProgress(id ,s_filename) {
	$.ajax({
              method : 'GET',
              url : 'fileOperations/getProgress.do',
              data : {"s_filename" : s_filename} 
          }).success(function(data){
//            console.info("data.progress="+data.progressvo.prog);
            try{
		 		document.getElementById(id+'progress-content').innerHTML=data.progressvo.prog;
            }catch (e) {}
      }); 
}
</script>
<!-- 预加载图片 -->
<img src = "${pageContext.request.contextPath}/images/ico_loading.gif"  style="display:none" />
<img src = "${pageContext.request.contextPath}/images/mini_send_Error.gif"  style="display:none" />

<div class="messageBoxContainer" ng-show="showMessageBox" >
	<div ng-class="{'rightOpBtn1':!isGroup, 'rightOpBtn2':isGroup}" ng-hide="currentChatObj.invalid" ng-click="setupContact(currentChatObj);"></div>
	<div class="leftOpBtn"></div>
	<!-- 聊天窗口主面板 -->
	<div class="chatMainPanel">
		<!-- 聊天窗口标题 -->
		<div class="chatTitle">
			<div class="chatNameWrap">
                <p class="chatName" ng-show="!currentChatObj.userCount">{{currentChatObj.name}}</p>
                <p class="chatName" ng-show="currentChatObj.userCount">{{currentChatObj.name}}({{currentChatObj.userCount|groupUserCountFormat}}人)</p>
            </div>
		</div>
		<!-- 聊天窗口消息滚动区 -->
		<div class="chatScorll"  id="chatScorll">
			<div id="chat_chatmsglist" class="chatContent" ctrl="1" style="top: 0px; position: absolute;">
				<div id="noMsgTip" class="noMsgIip" ng-show="messages.length==0">
					<div class="noMsgTipPic"></div> 
					<p>暂时没有新消息</p> 
				</div>
				<div ng-class="{me:message.msgType&&message.from==user.identifier,you:message.msgType&&message.from!=user.identifier}"
                     class="chatItem" ng-repeat="message in messages | messageSupplementary" on-message-finish-render>
                    <div class="systemTip" ng-if="message.commType">
                        <span >{{message.content}}</span>
                    </div>
                    <div class="r_time" ng-if="message.msgType && message.showMessageTime" >
                        <span class="timeBg"></span>
                            {{message.timestamp | dateFormat}}
                        <span class="timeBg"></span>
                    </div>
                    <div class="chatItemContent" ng-if="message.msgType">
						<img ng-if="message.avatar" class="avatar" ng-src="{{message.avatar|editAvatarFormat}}" alt="{{message.username}}" ng-click="showContactInfo(message.from)" title="{{message.fromName}}">
						<img ng-if="!message.avatar" class="avatar" src="${pageContext.request.contextPath}/images/oneps40.png" alt="{{message.username}}" ng-click="showContactInfo(message.from)"  title="{{message.fromName}}">
			            <div class="msgBox">
			            <div class="msgloading" ng-if="message.sendState==0&&message.msgType==11"></div>
			            <div style="float: left;margin: 7 4;" ng-if="message.sendState==2" title="重新发送" ng-click="doubleSend(message)">
			            	<img src="${pageContext.request.contextPath}/images/sendError.gif">
			            </div>
			                <p ng-if="isGroup&&message.from!=user.username" class="sendName">{{message.fromName}}</p>
			                <div class="cloud cloudText">
			                <!-- 文件消息 -->
			                <iframe frameborder="0" id="Iframe1" name="Iframe1" style="display: none;"></iframe>
			               	<div ng-if="message.msgType==18" class="filemessageStyle"> 
			               		<!-- <a ng-href="{{message.thumbContent}}">  -->
			               		<a href="#" ng-click="downloadFile(message.thumbContent)">
			               		<div class="fileleft fileIcon"> 
			               			<!-- <span class="fileImg"></span>-->
			               			<img ng-src="${pageContext.request.contextPath}/images/icon/file/9.png" style="height:40px;width:50px;margin-top: 10px;"/>
			               		</div> <!--<img src="fileOfZip" onerror="reLoadImg(this)" class="icon">--> 
			               	
			               		<div class="info fileleft"> 
			               			<p class="name">{{message.filename}}</p> 
			               			<p class="desc">{{message.filesize|betyFormat}}</p> 
			               			<div id="{{message.timestamp}}progressBar" style="display: none;">
										<div id="theMeter">
											<div id="{{message.timestamp}}progressBarBox"style="color: Silver; border-width: 1px; border-style: Solid; width: 85px;height:4px; TEXT-ALIGN: left">
											<div id="{{message.timestamp}}progressBarBoxContent"style="background-color: #3366FF; height: 4px; width: 0%; TEXT-ALIGN: left"></div>
											</div>
											<div id="{{message.timestamp}}progress-content" style="display: none;"></div>
										</div>
										</div>
			               				</div>
			               		</a> 
			               		</div>
			               		<!--  
                                 <a ng-if="message.msgType==18" ng-href="{{message.thumbContent}}">
                                     <img class="cloudPannel" ng-src="images/icon/file/9.ico" height="50px" width="60px"/>
                                     <p class="fileNameLength" title="{{message.filename}}" >{{message.filename}}</p>
                                 </a>-->
                                 <!-- 图片显示 -->
                                 <div ng-if="message.msgType==12">
                                 	<div class="imagemessageStyle">
			               				<img ng-if="message.sendstats == 0" src='${pageContext.request.contextPath}/images/send_default.png' style=" height: 70;width: 100;"/>
										<a ng-if="message.sendstats == 1" ng-href="{{message.sourceURL|imgUrlFormat}}" data-lightbox="example-set">
                                		<img class="cloudPannel"  ng-src="{{message.thumbContent}}" style="width:100px;max-height: 100px;min-height: 50px;"/>
                                		</a>
									</div>		       
			               				<div id="{{message.timestamp}}progressBar" style="display: none;margin-top: 3px;">
										<div id="theMeter">
											<div id="{{message.timestamp}}progressBarBox"style="color: Silver; border-width: 1px; border-style: Solid; width: 85px;height:4px; TEXT-ALIGN: left">
											<div id="{{message.timestamp}}progressBarBoxContent"style="background-color: #3366FF; height: 4px; width: 0%; TEXT-ALIGN: left"></div>
											</div>
											<div id="{{message.timestamp}}progress-content" style="display: none;"></div>
										</div>
										</div>
			               			</div>  
                                <!--语音消息
                                <div ng-if="message.msgType==13">
                                    <div class="cloud cloudVoice" ng-click="message.playing = !message.playing; playVoice(message); "style="width: 100px">
                                        <div class="cloudPannel" style="">
                                            <div class="sendStatus">
                                                <span class="second">{{message.playTime}}"</span>
                                            </div>
                                            <div class="cloudBody">
                                                <div class="cloudContent">
                                                    <span ng-class="{'icoVoice' : !message.playing, 'icoVoicePlaying' : message.playing}"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                -->
                                <div ng-if="message.msgType==13" class="cloudPannel" ng-show="showText"><pre style="white-space:pre-wrap"><font color="#c00000">系统提醒: 请登录移动端接收语音消息.</font></pre></div>
                                <div ng-if="message.msgType==11&&message.imgcontent==message.content" class="cloudPannel" ng-show="showText"><pre style="white-space:pre-wrap">{{message.content}}</pre></div>
                                <div ng-if="message.msgType==11&&message.imgcontent!=message.content" class="cloudPannel" ng-bind-html="message.imgcontent|to_trusted"></div>
                                <div class="clr"></div>
                            </div>
			            </div>
			        </div>
				</div>
			</div>
			<!-- 聊天窗口消息区滚动条 -->
			<div class="scrollbarBox" style="position: absolute; right: 0px; top: 0px; height: 100%;">
				<div class="scrollbar" style="display: none; position: absolute; right: 0px; top: 0px; opacity: 0;"></div>
			</div>
            <div id="drop_area" class="drop_area" ng-show="showDropArea">请将文件拖拽到此区域</div>
        </div>
		<!-- 聊天消息输入区 -->
		<div class="chat_editor" class="chatOperator lightBorder">
		    <div class="inputArea">
		        <div class="attach">
		            <div class="func expression">
		                 <img id="selectfeel" title="发送表情" src="${pageContext.request.contextPath}/images/feel.png" ng-click="showEmoji=!showEmoji;emojiType=1;" > 
		            </div>
		            <div class="func expression">
		                <img id="selectPic" title="发送文件" src="${pageContext.request.contextPath}/images/pic.png">
		                <input type="file" id="selectFile" style="display:none;" accept="/*"/>
		            </div>
		        </div>
		        <textarea type="text" class="messageInput lightBorder" id="contentTextarea" ng-model="content"  autofocus></textarea>
		        <script type="text/javascript">util.autoTextarea(document.getElementById('contentTextarea'), 0, 78);</script>
                <button class="chatSend" title="按Ctrl+Enter快速发送" ng-click="sendMessage();">发送</button>
		        <div class="cl"></div>
		    </div>
		    <jsp:include page="/pages/main/emoji_panel.jsp"/>
		</div>
	</div>
	<div class="chatDetailPanel">
	
	</div>
</div>