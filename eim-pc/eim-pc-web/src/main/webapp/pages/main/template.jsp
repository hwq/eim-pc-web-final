<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/ng-template" id="messageList.html">
    <no-message></no-message>
</script>

<script type="text/ng-template" id="noMessage.html">
    <div id="noMsgTip" class="noMsgIip" ng-show="{{noMessage}}"> 
		<div class="noMsgTipPic">{{messages}}</div> 
		<p>暂时没有新消息</p> 
	</div>
</script>

<script type="text/ng-template" id="organization.html">
	<div>
    	<a class="groupTitle pointer" ng-click="showGroups=!showGroups">
			<span class="avatar fl"><img src="${pageContext.request.contextPath}/images/department.jpg"></span>
        	<div class="left name" style="">{{organization.name}}</div>
        	<div class="clr"></div> 
	 	</a>
		<div class="groupTitleDetails" ng-show="showGroups">
        	<group ng-repeat="group in groups" group="group"></group>
        </div>
	 </div>
</script>
<script type="text/ng-template" id="group.html">
	<ul>
		<li class="titles titles-false" ng-click="showContacts=!showContacts;">
            {{group.name}}
        	<span>({{group.membercount}})</span>
        </li>
		<li ng-if="showContacts">
			<contact ng-repeat="contact in contacts" contact="contact"></contact>
		</li>
	</ul>
</script>
<script type="text/ng-template" id="contact.html">
	<a class="friendDetail" ng-click="personChat(contact)">
  		<span class="avatar" style="float: left;">
  			<img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
  			<img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
  		</span>
  		<div class="left name" style="">{{contact.name}}</div>
  			<p class="mystar" style="display:none"></p>
        <div class="clr"></div>
  	</a>
</script>