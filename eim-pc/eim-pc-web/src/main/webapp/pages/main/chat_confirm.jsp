<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="personalinfo" style="width: 400px;" ng-show="showConfrimChatBox">
    <div class="hd">
	    <h4 class="fl">详细资料</h4>
	    <span class="closeinfo" ng-click="hideChatBox();"></span>
	</div>
	<div class="bd">
	    <div class="imgAndName">
	        <p class="fl image">
	        	<img ng-if="confirmChatObj.avatar" ng-src="{{confirmChatObj.avatar|editAvatarFormat}}">
	            <img ng-if="!confirmChatObj.avatar" src="${pageContext.request.contextPath}/images/oneps40.png"> 
			</p>
	        <p class="fl psname">{{confirmChatObj.name}}</p>
	        <p class="fl pstip">{{confirmChatObj.post}}</p>
	        <p ng-class="{markStared:confirmChatObj.star, markStar:!confirmChatObj.star}" class="fr" 
	        ng-click="changeStar(confirmChatObj);" ng-if="confirmChatObj.name != user.name"></p>
	        <div class="cl"></div>
	    </div>
	    <p class="emailAddr">邮件地址：<span>{{confirmChatObj.email}}</span></p>
	    <p class="tel">联系电话：<span>{{confirmChatObj.telephone}}</span></p>
	    <p class="sendmsg">
	        <input type="button" value="发消息" ng-click="confirmChat(confirmChatObj);resetChatBoxStyle(0)">
	    </p>
	</div>
</div>