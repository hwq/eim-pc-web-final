<!-- 表情panel -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="emojiPanel" id="emojiPanel" ng-show="showEmoji">
    <div class="emojiTab">
        <ul class="faceTab">
            <li class="li2" ng-class="{true: 'cur', false: ''}[emojiType==1]" ng-click="showEmoji=true;emojiType=1"><a ></a></li>
            <li class="li3" ng-class="{true: 'cur', false: ''}[emojiType==2]" ng-click="showEmoji=true;emojiType=2"><a ></a></li>
            <li class="li4" ng-class="{true: 'cur', false: ''}[emojiType==3]" ng-click="showEmoji=true;emojiType=3"><a ></a></li>
        </ul>
    </div>
    <div id="faceWrap" class="faceWrap" >
        <ul class="expreCon" >
            <li class="bg2" ng-show="emojiType==1">
                <a title="高兴"     href="javascript:;" ><span class="emoji1f604" ></span></a>
                <a title="开心"     href="javascript:;" ><span class="emoji1f603" ></span></a>
                <a title="微笑"     href="javascript:;" ><span class="emoji1f600" ></span></a>
                <a title="可爱"     href="javascript:;" ><span class="emoji1f60a" ></span></a>
                <a title="害羞"     href="javascript:;" ><span class="emoji263a"  ></span></a>
                <a title="挤眼"     href="javascript:;" ><span class="emoji1f609" ></span></a>
                <a title="色"       href="javascript:;" ><span class="emoji1f60d" ></span></a>
                <a title="飞吻"     href="javascript:;" ><span class="emoji1f618" ></span></a>
                <a title="亲吻"     href="javascript:;" ><span class="emoji1f61a" ></span></a>
                <a title="亲亲"     href="javascript:;" ><span class="emoji1f617" ></span></a>
                <a title="笑吻"     href="javascript:;" ><span class="emoji1f619" ></span></a>
                <a title="调皮"     href="javascript:;" ><span class="emoji1f61c" ></span></a>
                <a title="赖皮"     href="javascript:;" ><span class="emoji1f61d" ></span></a>
                <a title="吐舌"     href="javascript:;" ><span class="emoji1f61b" ></span></a>
                <a title="发呆"     href="javascript:;" ><span class="emoji1f633" ></span></a>
                <a title="呲牙"     href="javascript:;" ><span class="emoji1f601" ></span></a>
                <a title="惆怅"     href="javascript:;" ><span class="emoji1f614" ></span></a> 

                <a title="思考"     href="javascript:;" ><span class="emoji1f60c" ></span></a>
                <a title="无感"     href="javascript:;" ><span class="emoji1f612" ></span></a>
                <a title="伤心"     href="javascript:;" ><span class="emoji1f61e" ></span></a>
                <a title="晕"       href="javascript:;" ><span class="emoji1f623" ></span></a>
                <a title="流泪"     href="javascript:;" ><span class="emoji1f622" ></span></a>
                <a title="笑抽"     href="javascript:;" ><span class="emoji1f602" ></span></a>
                <a title="大哭"     href="javascript:;" ><span class="emoji1f62d" ></span></a>
                <a title="瞌睡"     href="javascript:;" ><span class="emoji1f62a" ></span></a>
                <a title="流汗"     href="javascript:;" ><span class="emoji1f625" ></span></a>
                <a title="冷汗"     href="javascript:;" ><span class="emoji1f630" ></span></a>
                <a title="笑出汗"   href="javascript:;" ><span class="emoji1f605" ></span></a>
                <a title="汗"       href="javascript:;" ><span class="emoji1f613" ></span></a>
                <a title="疲惫"     href="javascript:;" ><span class="emoji1f629" ></span></a>
                <a title="累"       href="javascript:;" ><span class="emoji1f62b" ></span></a>
                <a title="衰"       href="javascript:;" ><span class="emoji1f628" ></span></a>
                <a title="恐怖"     href="javascript:;" ><span class="emoji1f631" ></span></a>
                <a title="生气"     href="javascript:;" ><span class="emoji1f620" ></span></a>

                <a title="发怒"     href="javascript:;" ><span class="emoji1f621" ></span></a>
                <a title="赢"       href="javascript:;" ><span class="emoji1f624" ></span></a>
                <a title="崩溃"     href="javascript:;" ><span class="emoji1f616" ></span></a>
                <a title="憨笑"     href="javascript:;" ><span class="emoji1f606" ></span></a>
                <a title="得意"     href="javascript:;" ><span class="emoji1f60b" ></span></a>
                <a title="感冒"     href="javascript:;" ><span class="emoji1f637" ></span></a>
                <a title="困"       href="javascript:;" ><span class="emoji1f60e" ></span></a>
                <a title="睡觉"     href="javascript:;" ><span class="emoji1f634" ></span></a>
                <a title="目眩"     href="javascript:;" ><span class="emoji1f635" ></span></a>
                <a title="混乱"     href="javascript:;" ><span class="emoji1f632" ></span></a>
                <a title="担心"     href="javascript:;" ><span class="emoji1f61f" ></span></a>
                <a title="皱眉"     href="javascript:;" ><span class="emoji1f626" ></span></a>
                <a title="痛苦"     href="javascript:;" ><span class="emoji1f627" ></span></a>
                <a title="笑鬼"     href="javascript:;" ><span class="emoji1f608" ></span></a>
                <a title="魔鬼"     href="javascript:;" ><span class="emoji1f47f" ></span></a>
                <a title="开口"     href="javascript:;" ><span class="emoji1f62e" ></span></a>
                <a title="鬼脸"     href="javascript:;" ><span class="emoji1f62c" ></span></a>

                <a title="脸"       href="javascript:;" ><span class="emoji1f610" ></span></a>
                <a title="困惑"     href="javascript:;" ><span class="emoji1f615" ></span></a>
                <a title="沉默"     href="javascript:;" ><span class="emoji1f62f" ></span></a>
                <a title="闭嘴"     href="javascript:;" ><span class="emoji1f636" ></span></a>
                <a title="无辜"     href="javascript:;" ><span class="emoji1f607" ></span></a>
                <a title="阴险"     href="javascript:;" ><span class="emoji1f60f" ></span></a>
                <a title="无语"     href="javascript:;" ><span class="emoji1f611" ></span></a>
                <a title="巫师"     href="javascript:;" ><span class="emoji1f472" ></span></a>
                <a title="厨师"     href="javascript:;" ><span class="emoji1f473" ></span></a>
                <a title="警察"     href="javascript:;" ><span class="emoji1f46e" ></span></a>
                <a title="建筑师"   href="javascript:;" ><span class="emoji1f477" ></span></a>
                <a title="士兵"     href="javascript:;" ><span class="emoji1f482" ></span></a>
                <a title="婴儿"     href="javascript:;" ><span class="emoji1f476" ></span></a>
                <a title="男孩"     href="javascript:;" ><span class="emoji1f466" ></span></a>
                <a title="女孩"     href="javascript:;" ><span class="emoji1f467" ></span></a>
                <a title="爸爸"     href="javascript:;" ><span class="emoji1f468" ></span></a>
                <a title="妈妈"     href="javascript:;" ><span class="emoji1f469" ></span></a>

                <a title="爷爷"     href="javascript:;" ><span class="emoji1f474" ></span></a>
                <a title="奶奶"     href="javascript:;" ><span class="emoji1f475" ></span></a>
                <a title="学生"     href="javascript:;" ><span class="emoji1f471" ></span></a>
                <a title="天使"     href="javascript:;" ><span class="emoji1f47c" ></span></a>
                <a title="公主"     href="javascript:;" ><span class="emoji1f478" ></span></a>
                <a title="笑脸猫"   href="javascript:;" ><span class="emoji1f63a" ></span></a>
                <a title="微笑猫"   href="javascript:;" ><span class="emoji1f638" ></span></a>
                <a title="色猫"     href="javascript:;" ><span class="emoji1f63b" ></span></a>
                <a title="亲亲猫"   href="javascript:;" ><span class="emoji1f63d" ></span></a>
                <a title="假笑猫"   href="javascript:;" ><span class="emoji1f63c" ></span></a>
                <a title="尖叫猫"   href="javascript:;" ><span class="emoji1f640" ></span></a>
                <a title="哭猫"     href="javascript:;" ><span class="emoji1f63f" ></span></a>
                <a title="笑哭猫"   href="javascript:;" ><span class="emoji1f639" ></span></a>
                <a title="撅嘴猫"   href="javascript:;" ><span class="emoji1f63e" ></span></a>
                <a title="怪物"     href="javascript:;" ><span class="emoji1f479" ></span></a>
                <a title="小妖精"   href="javascript:;" ><span class="emoji1f47a" ></span></a>
                <a title="非礼勿视" href="javascript:;" ><span class="emoji1f648" ></span></a>

                <a title="非礼勿听" href="javascript:;" ><span class="emoji1f649" ></span></a>
                <a title="非礼勿说" href="javascript:;" ><span class="emoji1f64a" ></span></a>
                <a title="骷髅"     href="javascript:;" ><span class="emoji1f480" ></span></a>
                <a title="外星人"   href="javascript:;" ><span class="emoji1f47d" ></span></a>
                <a title="便便"     href="javascript:;" ><span class="emoji1f4a9" ></span></a>
                <a title="火焰"     href="javascript:;" ><span class="emoji1f525" ></span></a>
                <a title="闪烁星星" href="javascript:;"  ><span class="emoji2728"  ></span></a>
                <a title="星星"     href="javascript:;" ><span class="emoji1f31f" ></span></a>
                <a title="头晕"     href="javascript:;" ><span class="emoji1f4ab" ></span></a>
                <a title="爆炸"     href="javascript:;" ><span class="emoji1f4a5" ></span></a>
                <a title="青筋"     href="javascript:;" ><span class="emoji1f4a2" ></span></a>
                <a title="水滴"     href="javascript:;" ><span class="emoji1f4a6" ></span></a>
                <a title="雨滴"     href="javascript:;" ><span class="emoji1f4a7" ></span></a>
                <a title="睡觉"     href="javascript:;" ><span class="emoji1f4a4" ></span></a>
                <a title="喷气"     href="javascript:;" ><span class="emoji1f4a8" ></span></a>
                <a title="耳朵"     href="javascript:;" ><span class="emoji1f442" ></span></a>
                <a title="眼睛"     href="javascript:;" ><span class="emoji1f440" ></span></a>

                <a title="鼻子"     href="javascript:;" ><span class="emoji1f443" ></span></a>
                <a title="舌头"     href="javascript:;" ><span class="emoji1f445" ></span></a>
                <a title="嘴唇"     href="javascript:;" ><span class="emoji1f444" ></span></a>
                <a title="强"       href="javascript:;" ><span class="emoji1f44d" ></span></a>
                <a title="鄙视"     href="javascript:;" ><span class="emoji1f44e" ></span></a>
                <a title="OK"       href="javascript:;" ><span class="emoji1f44c" ></span></a>
                <a title="出拳"     href="javascript:;" ><span class="emoji1f44a" ></span></a>
                <a title="拳头"     href="javascript:;"  ><span class="emoji270a"  ></span></a>
                <a title="胜利"     href="javascript:;"  ><span class="emoji270c"  ></span></a>
                <a title="手掌"     href="javascript:;" ><span class="emoji1f44b" ></span></a>
                <a title="停"       href="javascript:;"  ><span class="emoji270b"  ></span></a>
                <a title="双手"     href="javascript:;" ><span class="emoji1f450" ></span></a>
                <a title="向上"     href="javascript:;" ><span class="emoji1f446" ></span></a>
                <a title="向下"     href="javascript:;" ><span class="emoji1f447" ></span></a>
                <a title="向右"     href="javascript:;" ><span class="emoji1f449" ></span></a>
                <a title="向左"     href="javascript:;" ><span class="emoji1f448" ></span></a>
                <a title="不是我"   href="javascript:;" ><span class="emoji1f64c" ></span></a>

                <a title="拜佛 "    href="javascript:;" ><span class="emoji1f64f" ></span></a>
                <a title="第一"     href="javascript:;"  ><span class="emoji261d"  ></span></a>
                <a title="鼓掌"     href="javascript:;" ><span class="emoji1f44f" ></span></a>
                <a title="肌肉"     href="javascript:;" ><span class="emoji1f4aa" ></span></a>
                <a title="走路"     href="javascript:;" ><span class="emoji1f6b6" ></span></a>
                <a title="跑步"     href="javascript:;" ><span class="emoji1f3c3" ></span></a>
                <a title="美女"     href="javascript:;" ><span class="emoji1f483" ></span></a>
                <a title="情侣"     href="javascript:;" ><span class="emoji1f46b" ></span></a>
                <a title="家人"     href="javascript:;" ><span class="emoji1f46a" ></span></a>
                <a title="兄弟"     href="javascript:;" ><span class="emoji1f46c" ></span></a>
                <a title="姐妹"     href="javascript:;" ><span class="emoji1f46d" ></span></a>
                <a title="夫妻"     href="javascript:;" ><span class="emoji1f48f" ></span></a>
                <a title="夫妻同心" href="javascript:;" ><span class="emoji1f491" ></span></a>
                <a title="跳舞"     href="javascript:;" ><span class="emoji1f46f" ></span></a>
                <a title="抱头"     href="javascript:;" ><span class="emoji1f646" ></span></a>
                <a title="交叉"     href="javascript:;" ><span class="emoji1f645" ></span></a>
                <a title="摆手"     href="javascript:;" ><span class="emoji1f481" ></span></a>

                <a title="举手"     href="javascript:;" ><span class="emoji1f64b" ></span></a>
                <a title="洗头"     href="javascript:;" ><span class="emoji1f486" ></span></a>
                <a title="剪发"     href="javascript:;" ><span class="emoji1f487" ></span></a>
                <a title="美甲"     href="javascript:;" ><span class="emoji1f485" ></span></a>
                <a title="新娘"     href="javascript:;" ><span class="emoji1f470" ></span></a>
                <a title="撇嘴女人" href="javascript:;" ><span class="emoji1f64e" ></span></a>
                <a title="皱眉女人" href="javascript:;" ><span class="emoji1f64d" ></span></a>
                <a title="考试"     href="javascript:;" ><span class="emoji1f647" ></span></a>
                <a title="礼帽"     href="javascript:;" ><span class="emoji1f3a9" ></span></a>
                <a title="皇冠"     href="javascript:;" ><span class="emoji1f451" ></span></a>
                <a title="草帽"     href="javascript:;" ><span class="emoji1f452" ></span></a>
                <a title="板鞋"     href="javascript:;" ><span class="emoji1f45f" ></span></a>
                <a title="男鞋"     href="javascript:;" ><span class="emoji1f45e" ></span></a>
                <a title="女鞋"     href="javascript:;" ><span class="emoji1f461" ></span></a>
                <a title="高跟鞋"   href="javascript:;" ><span class="emoji1f460" ></span></a>
                <a title="靴子"     href="javascript:;" ><span class="emoji1f462" ></span></a>
                <a title="T恤"      href="javascript:;" ><span class="emoji1f455" ></span></a>

                <a title="衬衫"     href="javascript:;" ><span class="emoji1f454" ></span></a>
                <a title="女士上衣" href="javascript:;" ><span class="emoji1f45a" ></span></a>
                <a title="连衣裙"   href="javascript:;" ><span class="emoji1f457" ></span></a>
                <a title="运动上衣" href="javascript:;" ><span class="emoji1f3bd" ></span></a>
                <a title="裤子"     href="javascript:;" ><span class="emoji1f456" ></span></a>
                <a title="和服"     href="javascript:;" ><span class="emoji1f458" ></span></a>
                <a title="内衣"     href="javascript:;" ><span class="emoji1f459" ></span></a>
                <a title="公文包"   href="javascript:;" ><span class="emoji1f4bc" ></span></a>
                <a title="手提包"   href="javascript:;" ><span class="emoji1f45c" ></span></a>
                <a title="袋子"     href="javascript:;" ><span class="emoji1f45d" ></span></a>
                <a title="钱包"     href="javascript:;" ><span class="emoji1f45b" ></span></a>
                <a title="眼镜"     href="javascript:;" ><span class="emoji1f453" ></span></a>
                <a title="蝴蝶结"   href="javascript:;" ><span class="emoji1f380" ></span></a>
                <a title="雨伞"     href="javascript:;" ><span class="emoji1f302" ></span></a>
                <a title="口红"     href="javascript:;" ><span class="emoji1f484" ></span></a>
                <a title="黄心"     href="javascript:;" ><span class="emoji1f49b" ></span></a>
                <a title="蓝心"     href="javascript:;" ><span class="emoji1f499" ></span></a>

                <a title="紫心"     href="javascript:;" ><span class="emoji1f49c" ></span></a>
                <a title="绿心"     href="javascript:;" ><span class="emoji1f49a" ></span></a>
                <a title="爱心"     href="javascript:;"  ><span class="emoji2764"  ></span></a>
                <a title="心碎"     href="javascript:;" ><span class="emoji1f494" ></span></a>
                <a title="心动"     href="javascript:;" ><span class="emoji1f497" ></span></a>
                <a title="心跳"     href="javascript:;" ><span class="emoji1f493" ></span></a>
                <a title="心"       href="javascript:;" ><span class="emoji1f495" ></span></a>
                <a title="喜欢"     href="javascript:;" ><span class="emoji1f496" ></span></a>
                <a title="爱心"     href="javascript:;" ><span class="emoji1f49e" ></span></a>
                <a title="一见倾心" href="javascript:;" ><span class="emoji1f498" ></span></a>
                <a title="情书"     href="javascript:;" ><span class="emoji1f48c" ></span></a>
                <a title="吻"       href="javascript:;" ><span class="emoji1f48b" ></span></a>
                <a title="钻戒"     href="javascript:;" ><span class="emoji1f48d" ></span></a>
                <a title="宝石"     href="javascript:;" ><span class="emoji1f48e" ></span></a>
                <a title="个人"     href="javascript:;" ><span class="emoji1f464" ></span></a>
                <a title="多人"     href="javascript:;" ><span class="emoji1f465" ></span></a>
                <a title="消息"     href="javascript:;" ><span class="emoji1f4ac" ></span></a>

                <a title="脚印"     href="javascript:;" ><span class="emoji1f463" ></span></a>
                <a title="云"       href="javascript:;" ><span class="emoji1f4ad" ></span></a>
            </li>
            <li class="bg3" ng-show="emojiType==2">
                <a title="狗狗"     href="javascript:;" ><span class="emoji1f436" > </span></a>
                <a title="狼"       href="javascript:;" ><span class="emoji1f43a" > </span></a>
                <a title="猫咪"     href="javascript:;" ><span class="emoji1f431" > </span></a>
                <a title="灰鼠"     href="javascript:;" ><span class="emoji1f42d" > </span></a>
                <a title="花鼠"     href="javascript:;" ><span class="emoji1f439" > </span></a>
                <a title="兔子"     href="javascript:;" ><span class="emoji1f430" > </span></a>
                <a title="青蛙"     href="javascript:;" ><span class="emoji1f438" > </span></a>
                <a title="老虎"     href="javascript:;" ><span class="emoji1f42f" > </span></a>
                <a title="考拉"     href="javascript:;" ><span class="emoji1f428" > </span></a>
                <a title="灰熊"     href="javascript:;" ><span class="emoji1f43b" > </span></a>
                <a title="猪头"     href="javascript:;" ><span class="emoji1f437" > </span></a>
                <a title="猪鼻子"   href="javascript:;" ><span class="emoji1f43d" > </span></a>
                <a title="牛头"     href="javascript:;" ><span class="emoji1f42e" > </span></a>
                <a title="野猪"     href="javascript:;" ><span class="emoji1f417" > </span></a>
                <a title="猴子"     href="javascript:;" ><span class="emoji1f435" > </span></a>
                <a title="小猴"     href="javascript:;" ><span class="emoji1f412" > </span></a>
                <a title="小马"     href="javascript:;" ><span class="emoji1f434" > </span></a>

                <a title="绵羊"     href="javascript:;" ><span class="emoji1f411" > </span></a>
                <a title="大象"     href="javascript:;" ><span class="emoji1f418" > </span></a>
                <a title="熊猫"     href="javascript:;" ><span class="emoji1f43c" > </span></a>
                <a title="黑鸟"     href="javascript:;" ><span class="emoji1f427" > </span></a>
                <a title="灰鸟"     href="javascript:;" ><span class="emoji1f426" > </span></a>
                <a title="黄鸟"     href="javascript:;" ><span class="emoji1f424" > </span></a>
                <a title="小鸡"     href="javascript:;" ><span class="emoji1f425" > </span></a>
                <a title="孵小鸡"     href="javascript:;" ><span class="emoji1f423" > </span></a>
                <a title="公鸡"     href="javascript:;" ><span class="emoji1f414" > </span></a>
                <a title="蛇"       href="javascript:;" ><span class="emoji1f40d" > </span></a>
                <a title="乌龟"     href="javascript:;" ><span class="emoji1f422" > </span></a>
                <a title="毛毛虫"   href="javascript:;" ><span class="emoji1f41b" > </span></a>
                <a title="蜜蜂"     href="javascript:;" ><span class="emoji1f41d" > </span></a>
                <a title="蚂蚁"     href="javascript:;" ><span class="emoji1f41c" > </span></a>
                <a title="甲虫"     href="javascript:;" ><span class="emoji1f41e" > </span></a>
                <a title="蜗牛"     href="javascript:;" ><span class="emoji1f40c" > </span></a>
                <a title="章鱼"     href="javascript:;" ><span class="emoji1f419" > </span></a>

                <a title="贝壳"     href="javascript:;" ><span class="emoji1f41a" > </span></a>
                <a title="热带鱼"   href="javascript:;" ><span class="emoji1f420" > </span></a>
                <a title="鱼儿"     href="javascript:;" ><span class="emoji1f41f" > </span></a>
                <a title="海豚"     href="javascript:;" ><span class="emoji1f42c" > </span></a>
                <a title="鲸"       href="javascript:;" ><span class="emoji1f433" > </span></a>
                <a title="鲸鱼"     href="javascript:;" ><span class="emoji1f40b" > </span></a>
                <a title="奶牛"     href="javascript:;" ><span class="emoji1f404" > </span></a>
                <a title="羊"       href="javascript:;" ><span class="emoji1f40f" > </span></a>
                <a title="鼠"       href="javascript:;" ><span class="emoji1f400" > </span></a>
                <a title="水牛"     href="javascript:;" ><span class="emoji1f403" > </span></a>
                <a title="虎"       href="javascript:;" ><span class="emoji1f405" > </span></a>
                <a title="兔"       href="javascript:;" ><span class="emoji1f407" > </span></a>
                <a title="龙"       href="javascript:;" ><span class="emoji1f409" > </span></a>
                <a title="马"       href="javascript:;" ><span class="emoji1f40e" > </span></a>
                <a title="羊"       href="javascript:;" ><span class="emoji1f410" > </span></a>
                <a title="鸡"       href="javascript:;" ><span class="emoji1f413" > </span></a>
                <a title="狗"       href="javascript:;" ><span class="emoji1f415" > </span></a>

                <a title="猪"       href="javascript:;" ><span class="emoji1f416" > </span></a>
                <a title="小白鼠"   href="javascript:;" ><span class="emoji1f401" > </span></a>
                <a title="牛"       href="javascript:;" ><span class="emoji1f402" > </span></a>
                <a title="龙头"     href="javascript:;" ><span class="emoji1f432" > </span></a>
                <a title="河豚"     href="javascript:;" ><span class="emoji1f421" > </span></a>
                <a title="鳄鱼"     href="javascript:;" ><span class="emoji1f40a" > </span></a>
                <a title="骆驼"     href="javascript:;" ><span class="emoji1f42b" > </span></a>
                <a title="单峰骆驼" href="javascript:;" ><span class="emoji1f42a" > </span></a>
                <a title="豹"       href="javascript:;" ><span class="emoji1f406" > </span></a>
                <a title="猫"       href="javascript:;" ><span class="emoji1f408" > </span></a>
                <a title="贵宾犬"   href="javascript:;" ><span class="emoji1f429" > </span></a>
                <a title="爪印"     href="javascript:;" ><span class="emoji1f43e" > </span></a>
                <a title="康乃馨"   href="javascript:;" ><span class="emoji1f490" > </span></a>
                <a title="桃花"     href="javascript:;" ><span class="emoji1f338" > </span></a>
                <a title="郁金香"   href="javascript:;" ><span class="emoji1f337" > </span></a>
                <a title="四叶草"   href="javascript:;" ><span class="emoji1f340" > </span></a>
                <a title="玫瑰"     href="javascript:;" ><span class="emoji1f339" > </span></a>

                <a title="向日葵"   href="javascript:;" ><span class="emoji1f33b" > </span></a>
                <a title="鲜花"     href="javascript:;" ><span class="emoji1f33a" > </span></a>
                <a title="枫叶"     href="javascript:;" ><span class="emoji1f341" > </span></a>
                <a title="绿叶"     href="javascript:;" ><span class="emoji1f343" > </span></a>
                <a title="黄叶"     href="javascript:;" ><span class="emoji1f342" > </span></a>
                <a title="草"       href="javascript:;" ><span class="emoji1f33f" > </span></a>
                <a title="稻谷"     href="javascript:;" ><span class="emoji1f33e" > </span></a>
                <a title="蘑菇"     href="javascript:;" ><span class="emoji1f344" > </span></a>
                <a title="仙人掌"   href="javascript:;" ><span class="emoji1f335" > </span></a>
                <a title="椰树"     href="javascript:;" ><span class="emoji1f334" > </span></a>
                <a title="常青树"   href="javascript:;" ><span class="emoji1f332" > </span></a>
                <a title="落叶树"   href="javascript:;" ><span class="emoji1f333" > </span></a>
                <a title="栗子"     href="javascript:;" ><span class="emoji1f330" > </span></a>
                <a title="幼苗"     href="javascript:;" ><span class="emoji1f331" > </span></a>
                <a title="开花"     href="javascript:;" ><span class="emoji1f33c" > </span></a>
                <a title="地球"     href="javascript:;" ><span class="emoji1f310" > </span></a>
                <a title="太阳"     href="javascript:;" ><span class="emoji1f31e" > </span></a>

                <a title="满月脸"   href="javascript:;" ><span class="emoji1f31d" > </span></a>
                <a title="新月脸"   href="javascript:;" ><span class="emoji1f31a" > </span></a>
                <a title="新月"     href="javascript:;" ><span class="emoji1f311" > </span></a>
                <a title="上峨眉月" href="javascript:;" ><span class="emoji1f312" > </span></a>
                <a title="上玄月"   href="javascript:;" ><span class="emoji1f313" > </span></a>
                <a title="上凸月"   href="javascript:;" ><span class="emoji1f314" > </span></a>
                <a title="满月"     href="javascript:;" ><span class="emoji1f315" > </span></a>
                <a title="下凸月"   href="javascript:;" ><span class="emoji1f316" > </span></a>
                <a title="下玄月"   href="javascript:;" ><span class="emoji1f317" > </span></a>
                <a title="下峨眉月" href="javascript:;" ><span class="emoji1f318" > </span></a>
                <a title="上玄月脸" href="javascript:;" ><span class="emoji1f31c" > </span></a>
                <a title="下玄月脸" href="javascript:;" ><span class="emoji1f31b" > </span></a>
                <a title="月亮"     href="javascript:;" ><span class="emoji1f319" > </span></a>
                <a title="非洲"     href="javascript:;" ><span class="emoji1f30d" > </span></a>
                <a title="美洲"     href="javascript:;" ><span class="emoji1f30e" > </span></a>
                <a title="亚洲"     href="javascript:;" ><span class="emoji1f30f" > </span></a>
                <a title="火山"     href="javascript:;" ><span class="emoji1f30b" > </span></a>

                <a title="银河系"   href="javascript:;" ><span class="emoji1f30c" > </span></a>
                <%--<a title="流星"     href="javascript:;" ><span class="emoji_1f303" > </span></a>--%>
                <a title="星星"     href="javascript:;" ><span class="emoji2b50"  > </span></a>
                <a title="晴天"     href="javascript:;" ><span class="emoji2600"  > </span></a>
                <a title="多云"     href="javascript:;" ><span class="emoji26c5"  > </span></a>
                <a title="阴天"     href="javascript:;" ><span class="emoji2601"  > </span></a>
                <a title="闪电"     href="javascript:;" ><span class="emoji26a1"  > </span></a>
                <a title="雨天"     href="javascript:;" ><span class="emoji2614"  > </span></a>
                <a title="雪花"     href="javascript:;" ><span class="emoji2744"  > </span></a>
                <a title="雪人"     href="javascript:;" ><span class="emoji26c4"  > </span></a>
                <a title="热带风暴" href="javascript:;" ><span class="emoji1f300" > </span></a>
                <a title="雾霾"     href="javascript:;" ><span class="emoji1f301" > </span></a>
                <a title="彩虹"     href="javascript:;" ><span class="emoji1f308" > </span></a>
                <a title="海洋"     href="javascript:;" ><span class="emoji1f30a" > </span></a>
            </li>
            <li class="bg4" ng-show="emojiType==3">
                <a title="盆景"       href="javascript:;" ><span class="emoji1f38d" ></span></a>
                <a title="爱的礼物"   href="javascript:;" ><span class="emoji1f49d" ></span></a>
                <a title="婚礼"       href="javascript:;" ><span class="emoji1f38e" ></span></a>
                <a title="箱子"       href="javascript:;" ><span class="emoji1f392" ></span></a>
                <a title="学位帽"     href="javascript:;" ><span class="emoji1f393" ></span></a>
                <a title="鲤鱼旗"     href="javascript:;" ><span class="emoji1f38f" ></span></a>
                <a title="烟花"       href="javascript:;" ><span class="emoji1f386" ></span></a>
                <a title="烟火"       href="javascript:;" ><span class="emoji1f387" ></span></a>
                <a title="风铃"       href="javascript:;" ><span class="emoji1f390" ></span></a>
                <a title="夜色"       href="javascript:;" ><span class="emoji1f391" ></span></a>
                <a title="南瓜灯"     href="javascript:;" ><span class="emoji1f383" ></span></a>
                <a title="小鬼"       href="javascript:;" ><span class="emoji1f47b" ></span></a>
                <a title="圣诞老人"   href="javascript:;" ><span class="emoji1f385" ></span></a>
                <a title="圣诞树"     href="javascript:;" ><span class="emoji1f384" ></span></a>
                <a title="礼物"       href="javascript:;" ><span class="emoji1f381" ></span></a>
                <a title="七夕树"     href="javascript:;" ><span class="emoji1f38b" ></span></a>
                <a title="礼花"       href="javascript:;" ><span class="emoji1f389" ></span></a>
                <a title="纸屑球"     href="javascript:;" ><span class="emoji1f38a" ></span></a>
                <a title="气球"       href="javascript:;" ><span class="emoji1f388" ></span></a>
                <a title="日本旗"     href="javascript:;" ><span class="emoji1f38c" ></span></a>
                <a title="水晶球"     href="javascript:;" ><span class="emoji1f52e" ></span></a>
                <a title="电影摄影机" href="javascript:;" ><span class="emoji1f3a5" ></span></a>
                <a title="相机"       href="javascript:;" ><span class="emoji1f4f7" ></span></a>
                <a title="摄像机"     href="javascript:;" ><span class="emoji1f4f9" ></span></a>
                <a title="磁带"       href="javascript:;" ><span class="emoji1f4fc" ></span></a>
                <a title="光盘"       href="javascript:;" ><span class="emoji1f4bf" ></span></a>
                <a title="CD"         href="javascript:;" ><span class="emoji1f4c0" ></span></a>
                <a title="光碟"       href="javascript:;" ><span class="emoji1f4bd" ></span></a>
                <a title="软盘"       href="javascript:;" ><span class="emoji1f4be" ></span></a>
                <a title="计算机"     href="javascript:;" ><span class="emoji1f4bb" ></span></a>
                <a title="手机"       href="javascript:;" ><span class="emoji1f4f1" ></span></a>
                <a title="电话"       href="javascript:;" ><span class="emoji260e" ></span></a>
                <a title="听筒"       href="javascript:;" ><span class="emoji1f4de" ></span></a>
                <a title="磁带"       href="javascript:;" ><span class="emoji1f4df" ></span></a>
                <a title="传真"       href="javascript:;" ><span class="emoji1f4e0" ></span></a>
                <a title="广播"       href="javascript:;" ><span class="emoji1f4e1" ></span></a>
                <a title="电视机"     href="javascript:;" ><span class="emoji1f4fa" ></span></a>
                <a title="收音机"     href="javascript:;" ><span class="emoji1f4fb" ></span></a>
                <a title="#号"        href="javascript:;" ><span class="emoji1f508" ></span></a>
                <a title="扬声器"     href="javascript:;" ><span class="emoji1f509" ></span></a>
                <a title="声音"       href="javascript:;" ><span class="emoji1f50a" ></span></a>
                <a title="静音"       href="javascript:;" ><span class="emoji1f507" ></span></a>
                <a title="铃铛"       href="javascript:;" ><span class="emoji1f514" ></span></a>
                <a title="无音铃铛"   href="javascript:;" ><span class="emoji1f515" ></span></a>
                <a title="大喇叭"     href="javascript:;" ><span class="emoji1f4e2" ></span></a>
                <a title="小喇叭"     href="javascript:;" ><span class="emoji1f4e3" ></span></a>
                <a title="沙漏"       href="javascript:;" ><span class="emoji23f3" ></span></a>
                <a title="流沙"       href="javascript:;" ><span class="emoji231b" ></span></a>
                <a title="闹钟"       href="javascript:;" ><span class="emoji23f0" ></span></a>
                <a title="手表"       href="javascript:;" ><span class="emoji231a" ></span></a>
                <a title="开源"       href="javascript:;" ><span class="emoji1f513" ></span></a>
                <a title="锁头"       href="javascript:;" ><span class="emoji1f512" ></span></a>
                <a title="笔锁"       href="javascript:;" ><span class="emoji1f50f" ></span></a>
                <a title="锁"         href="javascript:;" ><span class="emoji1f510" ></span></a>
                <a title="钥匙"       href="javascript:;" ><span class="emoji1f511" ></span></a>
                <a title="右放大镜"   href="javascript:;" ><span class="emoji1f50e" ></span></a>
                <a title="灯泡"       href="javascript:;" ><span class="emoji1f4a1" ></span></a>
                <a title="手电筒"     href="javascript:;" ><span class="emoji1f526" ></span></a>
                <a title="高亮度"     href="javascript:;" ><span class="emoji1f506" ></span></a>
                <a title="低亮度"     href="javascript:;" ><span class="emoji1f505" ></span></a>
                <a title="电插头"     href="javascript:;" ><span class="emoji1f50c" ></span></a>
                <a title="电池"       href="javascript:;" ><span class="emoji1f50b" ></span></a>
                <a title="放大镜"     href="javascript:;" ><span class="emoji1f50d" ></span></a>
                <a title="浴缸"       href="javascript:;" ><span class="emoji1f6c1" ></span></a>
                <a title="沐浴"       href="javascript:;" ><span class="emoji1f6c0" ></span></a>
                <a title="淋浴"       href="javascript:;" ><span class="emoji1f6bf" ></span></a>
                <a title="马桶"       href="javascript:;" ><span class="emoji1f6bd" ></span></a>
                <a title="扳手"       href="javascript:;" ><span class="emoji1f527" ></span></a>
                <a title="螺丝"       href="javascript:;" ><span class="emoji1f529" ></span></a>
                <a title="锤子"       href="javascript:;" ><span class="emoji1f528" ></span></a>
                <a title="门"         href="javascript:;" ><span class="emoji1f6aa" ></span></a>
                <a title="香烟"       href="javascript:;" ><span class="emoji1f6ac" ></span></a>
                <a title="炸弹"       href="javascript:;" ><span class="emoji1f4a3" ></span></a>
                <a title="手枪"       href="javascript:;" ><span class="emoji1f52b" ></span></a>
                <a title="水果刀"     href="javascript:;" ><span class="emoji1f52a" ></span></a>
                <a title="药丸"       href="javascript:;" ><span class="emoji1f48a" ></span></a>
                <a title="针管"       href="javascript:;" ><span class="emoji1f489" ></span></a>
                <a title="金钱"       href="javascript:;" ><span class="emoji1f4b0" ></span></a>
                <a title="日元"       href="javascript:;" ><span class="emoji1f4b4" ></span></a>
                <a title="美元"       href="javascript:;" ><span class="emoji1f4b5" ></span></a>
                <a title="英镑"       href="javascript:;" ><span class="emoji1f4b7" ></span></a>
                <a title="欧元"       href="javascript:;" ><span class="emoji1f4b6" ></span></a>
                <a title="信用卡"     href="javascript:;" ><span class="emoji1f4b3" ></span></a>
                <a title="钱的翅膀"   href="javascript:;" ><span class="emoji1f4b8" ></span></a>
                <a title="来电"       href="javascript:;" ><span class="emoji1f4f2" ></span></a>
                <a title="电子邮件"   href="javascript:;" ><span class="emoji1f4e7" ></span></a>
                <a title="收件箱"     href="javascript:;" ><span class="emoji1f4e5" ></span></a>
                <a title="发件箱"     href="javascript:;" ><span class="emoji1f4e4" ></span></a>
                <a title="信封"       href="javascript:;" ><span class="emoji2709" ></span></a>
                <a title="收信"       href="javascript:;" ><span class="emoji1f4e9" ></span></a>
                <a title="来信"       href="javascript:;" ><span class="emoji1f4e8" ></span></a>
                <a title="号角"       href="javascript:;" ><span class="emoji1f4ef" ></span></a>
                <a title="邮箱"       href="javascript:;" ><span class="emoji1f4eb" ></span></a>
                <a title="关闭邮箱"   href="javascript:;" ><span class="emoji1f4ea" ></span></a>
                <a title="邮件箱"     href="javascript:;" ><span class="emoji1f4ec" ></span></a>
                <a title="空邮箱"     href="javascript:;" ><span class="emoji1f4ed" ></span></a>
                <a title="邮筒"       href="javascript:;" ><span class="emoji1f4ee" ></span></a>
                <a title="收纳箱"     href="javascript:;" ><span class="emoji1f4e6" ></span></a>
                <a title="备忘录"     href="javascript:;" ><span class="emoji1f4dd" ></span></a>
                <a title="页面对"     href="javascript:;" ><span class="emoji1f4c4" ></span></a>
                <a title="页卷曲"     href="javascript:;" ><span class="emoji1f4c3" ></span></a>
                <a title="书签"       href="javascript:;" ><span class="emoji1f4d1" ></span></a>
                <a title="条形图"     href="javascript:;" ><span class="emoji1f4ca" ></span></a>
                <a title="向上趋势"   href="javascript:;" ><span class="emoji1f4c8" ></span></a>
                <a title="向下趋势"   href="javascript:;" ><span class="emoji1f4c9" ></span></a>
                <a title="滚动"       href="javascript:;" ><span class="emoji1f4dc" ></span></a>
                <a title="剪切"       href="javascript:;" ><span class="emoji1f4cb" ></span></a>
                <a title="日历"       href="javascript:;" ><span class="emoji1f4c5" ></span></a>
                <a title="日期"       href="javascript:;" ><span class="emoji1f4c6" ></span></a>
                <a title="索引"       href="javascript:;" ><span class="emoji1f4c7" ></span></a>
                <a title="文件夹"     href="javascript:;" ><span class="emoji1f4c1" ></span></a>
                <a title="开文件夹"   href="javascript:;" ><span class="emoji1f4c2" ></span></a>
                <a title="剪刀"       href="javascript:;" ><span class="emoji2702" ></span></a>
                <a title="图钉"       href="javascript:;" ><span class="emoji1f4cc" ></span></a>
                <a title="回形针"     href="javascript:;" ><span class="emoji1f4ce" ></span></a>
                <a title="钢笔"       href="javascript:;" ><span class="emoji2712" ></span></a>
                <a title="铅笔"       href="javascript:;" ><span class="emoji270f" ></span></a>
                <a title="直尺"       href="javascript:;" ><span class="emoji1f4cf" ></span></a>
                <a title="三角板"     href="javascript:;" ><span class="emoji1f4d0" ></span></a>
                <a title="红色的书"   href="javascript:;" ><span class="emoji1f4d5" ></span></a>
                <a title="绿色的书"   href="javascript:;" ><span class="emoji1f4d7" ></span></a>
                <a title="蓝色的书"   href="javascript:;" ><span class="emoji1f4d8" ></span></a>
                <a title="橙色的书"   href="javascript:;" ><span class="emoji1f4d9" ></span></a>
                <a title="灰笔记本"   href="javascript:;" ><span class="emoji1f4d3" ></span></a>
                <a title="黄笔记本"   href="javascript:;" ><span class="emoji1f4d4" ></span></a>
                <a title="账本"       href="javascript:;" ><span class="emoji1f4d2" ></span></a>
                <a title="书本"       href="javascript:;" ><span class="emoji1f4da" ></span></a>
                <a title="看书"       href="javascript:;" ><span class="emoji1f4d6" ></span></a>
                <a title="书签"       href="javascript:;" ><span class="emoji1f516" ></span></a>
                <a title="徽章"       href="javascript:;" ><span class="emoji1f4db" ></span></a>
                <a title="显微镜"     href="javascript:;" ><span class="emoji1f52c" ></span></a>
                <a title="望远镜"     href="javascript:;" ><span class="emoji1f52d" ></span></a>
                <a title="报纸"       href="javascript:;" ><span class="emoji1f4f0" ></span></a>
                <a title="画画"       href="javascript:;" ><span class="emoji1f3a8" ></span></a>
                <a title="电影"       href="javascript:;" ><span class="emoji1f3ac" ></span></a>
                <a title="麦克风"     href="javascript:;" ><span class="emoji1f3a4" ></span></a>
                <a title="耳机"       href="javascript:;" ><span class="emoji1f3a7" ></span></a>
                <a title="乐谱"       href="javascript:;" ><span class="emoji1f3bc" ></span></a>
                <a title="音符"       href="javascript:;" ><span class="emoji1f3b5" ></span></a>
                <a title="乐符"       href="javascript:;" ><span class="emoji1f3b6" ></span></a>
                <a title="键盘乐器"   href="javascript:;" ><span class="emoji1f3b9" ></span></a>
                <a title="小提琴"     href="javascript:;" ><span class="emoji1f3bb" ></span></a>
                <a title="喇叭"       href="javascript:;" ><span class="emoji1f3ba" ></span></a>
                <a title="萨克斯"     href="javascript:;" ><span class="emoji1f3b7" ></span></a>
                <a title="吉他"       href="javascript:;" ><span class="emoji1f3b8" ></span></a>
                <a title="火星人"     href="javascript:;" ><span class="emoji1f47e" ></span></a>
                <a title="游戏机"     href="javascript:;" ><span class="emoji1f3ae" ></span></a>
                <a title="黑色小丑"   href="javascript:;" ><span class="emoji1f0cf" ></span></a>
                <a title="扑克"       href="javascript:;" ><span class="emoji1f3b4" ></span></a>
                <a title="麻将"       href="javascript:;" ><span class="emoji1f004" ></span></a>
                <a title="骰子"       href="javascript:;" ><span class="emoji1f3b2" ></span></a>
                <a title="飞镖"       href="javascript:;" ><span class="emoji1f3af" ></span></a>
                <a title="棕橄榄球"     href="javascript:;" ><span class="emoji1f3c8" ></span></a>
                <a title="篮球"       href="javascript:;" ><span class="emoji1f3c0" ></span></a>
                <a title="足球"       href="javascript:;" ><span class="emoji26bd" ></span></a>
                <a title="棒球"       href="javascript:;" ><span class="emoji26be" ></span></a>
                <a title="网球"       href="javascript:;" ><span class="emoji1f3be" ></span></a>
                <a title="桌球"       href="javascript:;" ><span class="emoji1f3b1" ></span></a>
                <a title="橙橄榄球"     href="javascript:;" ><span class="emoji1f3c9" ></span></a>
                <a title="保龄球"     href="javascript:;" ><span class="emoji1f3b3" ></span></a>
                <a title="高尔夫"     href="javascript:;" ><span class="emoji26f3" ></span></a>
                <a title="骑山地车"   href="javascript:;" ><span class="emoji1f6b5" ></span></a>
                <a title="骑自行车"   href="javascript:;" ><span class="emoji1f6b4" ></span></a>
                <a title="格子旗"     href="javascript:;" ><span class="emoji1f3c1" ></span></a>
                <a title="骑马"       href="javascript:;" ><span class="emoji1f3c7" ></span></a>
                <a title="奖杯"       href="javascript:;" ><span class="emoji1f3c6" ></span></a>
                <a title="滑雪板"     href="javascript:;" ><span class="emoji1f3bf" ></span></a>
                <a title="滑雪"       href="javascript:;" ><span class="emoji1f3c2" ></span></a>
                <a title="游泳"       href="javascript:;" ><span class="emoji1f3ca" ></span></a>
                <a title="冲浪"       href="javascript:;" ><span class="emoji1f3c4" ></span></a>
                <a title="钓鱼"       href="javascript:;" ><span class="emoji1f3a3" ></span></a>
                <a title="咖啡"       href="javascript:;" ><span class="emoji2615" ></span></a>
                <a title="绿茶"       href="javascript:;" ><span class="emoji1f375" ></span></a>
                <a title="米酒"       href="javascript:;" ><span class="emoji1f376" ></span></a>
                <a title="牛奶"       href="javascript:;" ><span class="emoji1f37c" ></span></a>
                <a title="啤酒"       href="javascript:;" ><span class="emoji1f37a" ></span></a>
                <a title="对饮"       href="javascript:;" ><span class="emoji1f37b" ></span></a>
                <a title="鸡尾酒"     href="javascript:;" ><span class="emoji1f378" ></span></a>
                <a title="果汁"       href="javascript:;" ><span class="emoji1f379" ></span></a>
                <a title="红酒"       href="javascript:;" ><span class="emoji1f377" ></span></a>
                <a title="刀叉"       href="javascript:;" ><span class="emoji1f374" ></span></a>
                <a title="披萨"       href="javascript:;" ><span class="emoji1f355" ></span></a>
                <a title="汉堡"       href="javascript:;" ><span class="emoji1f354" ></span></a>
                <a title="薯条"       href="javascript:;" ><span class="emoji1f35f" ></span></a>
                <a title="鸡腿"       href="javascript:;" ><span class="emoji1f357" ></span></a>
                <a title="肉骨"       href="javascript:;" ><span class="emoji1f356" ></span></a>
                <a title="炸酱面"     href="javascript:;" ><span class="emoji1f35d" ></span></a>
                <a title="咖喱饭"     href="javascript:;" ><span class="emoji1f35b" ></span></a>
                <a title="炸河虾"     href="javascript:;" ><span class="emoji1f364" ></span></a>
                <a title="饭盒"       href="javascript:;" ><span class="emoji1f371" ></span></a>
                <a title="寿司"       href="javascript:;" ><span class="emoji1f363" ></span></a>
                <a title="鱼饼"       href="javascript:;" ><span class="emoji1f365" ></span></a>
                <a title="饭团"       href="javascript:;" ><span class="emoji1f359" ></span></a>
                <a title="点心"       href="javascript:;" ><span class="emoji1f358" ></span></a>
                <a title="米饭"       href="javascript:;" ><span class="emoji1f35a" ></span></a>
                <a title="面条"       href="javascript:;" ><span class="emoji1f35c" ></span></a>
                <a title="汤"         href="javascript:;" ><span class="emoji1f372" ></span></a>
                <a title="关东煮"     href="javascript:;" ><span class="emoji1f362" ></span></a>
                <a title="丸子"       href="javascript:;" ><span class="emoji1f361" ></span></a>
                <a title="鸡蛋"       href="javascript:;" ><span class="emoji1f373" ></span></a>
                <a title="面包"       href="javascript:;" ><span class="emoji1f35e" ></span></a>
                <a title="炸圈饼"     href="javascript:;" ><span class="emoji1f369" ></span></a>
                <a title="蛋奶冻"     href="javascript:;" ><span class="emoji1f36e" ></span></a>
                <a title="甜筒"       href="javascript:;" ><span class="emoji1f366" ></span></a>
                <a title="冰淇淋"     href="javascript:;" ><span class="emoji1f368" ></span></a>
                <a title="刨冰"       href="javascript:;" ><span class="emoji1f367" ></span></a>
                <a title="生日蛋糕"   href="javascript:;" ><span class="emoji1f382" ></span></a>
                <a title="蛋糕"       href="javascript:;" ><span class="emoji1f370" ></span></a>
                <a title="饼干"       href="javascript:;" ><span class="emoji1f36a" ></span></a>
                <a title="巧克力"     href="javascript:;" ><span class="emoji1f36b" ></span></a>
                <a title="糖果"       href="javascript:;" ><span class="emoji1f36c" ></span></a>
                <a title="棒棒糖"     href="javascript:;" ><span class="emoji1f36d" ></span></a>
                <a title="蜂蜜罐"     href="javascript:;" ><span class="emoji1f36f" ></span></a>
                <a title="苹果"       href="javascript:;" ><span class="emoji1f34e" ></span></a>
                <a title="青苹果"     href="javascript:;" ><span class="emoji1f34f" ></span></a>
                <a title="橘子"       href="javascript:;" ><span class="emoji1f34a" ></span></a>
                <a title="柠檬"       href="javascript:;" ><span class="emoji1f34b" ></span></a>
                <a title="樱桃"       href="javascript:;" ><span class="emoji1f352" ></span></a>
                <a title="葡萄"       href="javascript:;" ><span class="emoji1f347" ></span></a>
                <a title="西瓜"       href="javascript:;" ><span class="emoji1f349" ></span></a>
                <a title="草莓"       href="javascript:;" ><span class="emoji1f353" ></span></a>
                <a title="水蜜桃"     href="javascript:;" ><span class="emoji1f351" ></span></a>
                <a title="甜瓜"       href="javascript:;" ><span class="emoji1f348" ></span></a>
                <a title="香蕉"       href="javascript:;" ><span class="emoji1f34c" ></span></a>
                <a title="梨子"       href="javascript:;" ><span class="emoji1f350" ></span></a>
                <a title="菠萝"       href="javascript:;" ><span class="emoji1f34d" ></span></a>
                <a title="甘薯"       href="javascript:;" ><span class="emoji1f360" ></span></a>
                <a title="茄子"       href="javascript:;" ><span class="emoji1f346" ></span></a>
                <a title="西红柿"     href="javascript:;" ><span class="emoji1f345" ></span></a>
                <a title="玉米"       href="javascript:;" ><span class="emoji1f33d" ></span></a>
            </li> 
        </ul>            
    </div>
</div> 