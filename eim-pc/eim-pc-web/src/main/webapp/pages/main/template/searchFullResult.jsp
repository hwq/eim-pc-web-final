<%--
  Created by IntelliJ IDEA.
  User: yanyuan
  Date: 2015/1/21
  Time: 19:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="scroller" ng-show="searchCondition != null && searchCondition.length > 0 ">
    <p class="searchPersonTitle personTitleLeft" ng-show="searchResult['contacts'].length > 0">联系人</p>
    <a class="friendDetail" ng-repeat="contact in searchResult['contacts'] | orderBy : 'desc' : 'name' " ng-click="clickSearchResultObj(contact);">
                        <span class="avatar" style="float: left;">
                            <img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
                            <img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
                        </span>
        <div class="left name" ng-if="!contact.desc" >
            <div class="searchResult" style="overflow:hidden;text-overflow: ellipsis;">{{contact.name}}</div>
        </div>
        <div class="left name" style="margin-top: 5px" ng-if="contact.desc" >
            <div style="overflow:hidden;text-overflow: ellipsis;">{{contact.name}}</div>
            <div class="searchResult desc" style="overflow:hidden;text-overflow: ellipsis;" ng-if="contact.desc" title="{{contact.desc}}">
                {{contact.desc}}
            </div>
        </div>
        <p class="mystar" style="display:none"></p>
        <div class="clr"></div>
    </a>
    <p class="searchPersonTitle personTitleLeft" ng-show="searchResult['groups'].length > 0">群组</p>
    <a class="friendDetail" ng-repeat="group in searchResult['groups'] | orderBy : 'name' " ng-click="clickSearchResultObj(group);">
                        <span class="avatar" style="float: left;">
                            <img ng-if="group.avatar" ng-src="{{group.avatar}}">
                            <img ng-if="!group.avatar" src="${pageContext.request.contextPath}/images/goup_default.png">
                        </span>
        <div class="left name" ng-if="!group.desc" >
            <div class="searchResult" style="overflow:hidden;text-overflow: ellipsis;">{{group.name}}</div>
        </div>
        <div class="left name" style="margin-top: 5px" ng-if="group.desc" >
            <div style="overflow:hidden;text-overflow: ellipsis;">{{group.name}}</div>
            <div class="searchResult desc" style="overflow:hidden;text-overflow: ellipsis;" ng-if="group.desc">
                {{group.desc}}
            </div>
        </div>
        <p class="mystar" style="display:none"></p>
        <div class="clr"></div>
    </a>
    <div style="padding:20px 0;font-size: 12px"
         ng-show="searchResult['contacts'].length == 0 && searchResult['groups'].length == 0"><span>找不到匹配的信息</span>
    </div>
</div>