<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="starFriends">
    <span class="star">星标联系人</span>
    <a class="friendDetail" ng-repeat="(key,contact) in starContacts" ng-if="contact!=null" ng-click="personChat(contact)">
        <div class="avatar_wrap">
            <img width="40px" height="40px" ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
            <img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
        </div>
        <div class="info">
            <div class="nickName">
                <div class="left name" style="">{{contact.name}}</div>
                <p class="desc">{{contact.post}}</p>
            </div>
        </div>
    </a>
</div>
