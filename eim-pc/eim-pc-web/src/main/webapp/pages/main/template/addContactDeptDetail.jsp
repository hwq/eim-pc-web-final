<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<li ng-if="showDetail">
    <a class="friendDetail" ng-click="addGroupPerson(contact.identifier,$event);"
       ng-repeat="contact in contacts  |  getContactsByDeptId:deptId | orderBy:['sort', 'pinyin'] | getContactStatus : groupContactIderfiers : groupPersons">
        <span ng-class="{'checked' : contact.isChecked, 'check' : !contact.isChecked}"
              id="span_{{contact.identifier}}"></span>
                                    <span class="avatar" style="float: left;">
                                        <img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
                                        <img ng-if="!contact.avatar"
                                             src="${pageContext.request.contextPath}/images/oneps40.png">
                                    </span>

        <div class="left name" style="">{{contact.name}}</div>
        <p class="mystar" style="display:none"></p>

        <div class="clr"></div>
    </a>
</li>
<li ng-if="showDetail">
    <ul class="deptTree" ng-repeat="dept in organizations | getDepts:deptId | orderBy:['sort','identifier']  "
        ng-init="showDetail=false">
        <li class="titles titles-{{!showDetail}}" ng-click="showDetail=!showDetail;deptId=dept.identifier;getChildren($event);">
            {{dept.name}}
            <span>({{dept.membercount}})</span>
        </li>
    </ul>
</li>