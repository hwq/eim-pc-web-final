<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div ng-repeat="organization in organizations | getCompanies | orderBy:['sort','identifier'] " ng-init="showDepts = false">
    <a class="groupTitle pointer" ng-click="showDepts=!showDepts;deptId=organization.identifier">
        <span class="avatar fl"><img src="${pageContext.request.contextPath}/images/department.jpg"></span>
        <div class="left name" style="">{{organization.name}}</div>
        <div class="clr"></div>
    </a>
    <div class="groupTitleDetails" ng-show="showDepts">
        <ul class = "deptTree" ng-repeat="dept in organizations | getDepts:deptId | orderBy:['sort','identifier'] " ng-init="showDetail=false">
            <li class="titles titles-{{!showDetail}}" ng-click="showDetail=!showDetail;deptId=dept.identifier;getChildren($event);">
                {{dept.name}}
                <span>({{dept.membercount}})</span>
            </li>
        </ul>
    </div>
</div>