<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="starFriends">
    <span class="star">星标联系人</span>
    <a class="friendDetail" ng-click="addGroupPerson(contact.identifier,$event);"
       ng-repeat="(key,contact) in starContacts | orderBy:['sort', 'pinyin'] | getContactStatus : groupContactIderfiers : groupPersons">
            <span ng-class="{'checked' : contact.isChecked, 'check' : !contact.isChecked}"
                  id="span_{{contact.identifier}}"></span>
            <span class="avatar" style="float: left;">
                <img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
                <img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
            </span>
        <div class="left name" style="margin-top: 5px">
            <div style="overflow:hidden;text-overflow: ellipsis;">{{contact.name}}</div>
            <div class="desc" style="overflow:hidden;text-overflow: ellipsis;">
                {{contact.post}}
            </div>
        </div>
        <p class="mystar" style="display:none"></p>
        <div class="clr"></div>
    </a>
</div>
