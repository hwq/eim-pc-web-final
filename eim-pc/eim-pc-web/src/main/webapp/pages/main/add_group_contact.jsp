<%--
  Created by IntelliJ IDEA.
  User: peguinDance
  Date: 2014/12/4
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="addParticipants" ng-show="showAddGroupContactBox">
    <div class="hd">
        <h4 class="fl">添加人员</h4>
        <!-- <h4 class="fl">添加参与人</h4> -->
        <span class="closeinfo" ng-click="closeAddContactBox()"></span>
        <div class="cl"></div>
    </div>
    <div class="inputContent">
        <input type="text" class="left" ng-model="groupCondition" placeholder="搜索" ng-change="groupSearch()"/>
        <span class="search left"></span>
        <span class="SearchClean left" style="right: 20px" ng-show="groupCondition.length>0 " ng-click="groupCondition='' "></span>
    </div>
    <div class="addContactListBox" ng-if="showAddGroupContactBox">
        <div>
            <p class="offsetTopHelp"></p>
            <p class="searchPersonTitle">联系人</p>
        </div>
        <!-- organization tree -->
        <div ng-class="{'moveUp':groupPersons.length>0}"  ng-if = "groupCondition == null || groupCondition.length == 0 ">
            <div add-group-contact-org-tree></div>
            <ng-include src="'${pageContext.request.contextPath}/pages/main/template/addGroupContactStarFriends.jsp'"></ng-include>
        </div>
        <div class="groupTitleDetails" ng-class="{'moveUp':groupPersons.length>0}"  ng-if="groupCondition != null && groupCondition.length > 0 ">
            <a class="friendDetail"
               ng-repeat="contact in searchGroupAddressResult | orderBy:['sort', 'pinyin'] |  getContactStatus : groupContactIderfiers : groupPersons "
               ng-click="addGroupPerson(contact.identifier,$event);"  >
                <span class="checked" ng-show="contact.isExist"></span>
                <span ng-class="{'checked' : contact.isChecked, 'check' : !contact.isChecked}" ng-show="!contact.isExist" id="span_search_{{contact.identifier}}"></span>
					  		<span class="avatar" style="float: left;">
					  			<img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
					  			<img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
					  		</span>
                <div class="left name">
                    <div class="searchResult" style="overflow:hidden;text-overflow: ellipsis;">{{contact.name}}</div>
                </div>
                <p class="mystar" style="display:none"></p>
                <div class="clr"></div>
            </a>
        </div>
    </div>
    <!-- search result -->
    <div class="numadd" ng-if="groupPersons.length>0"  ng-mouseover="showDelete=true" ng-mouseleave="showDelete=false">
        <div class="lbtn"></div>
        <div class="addcontactimg">
            <ul id="adimg">
                <li ng-repeat="contact in groupPersons | toContacts">
                    <img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}" title="{{contact.name}}">
                    <img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png" title="{{contact.name}}">
                    <span class="delete" ng-show="showDelete" ng-click="delGroupPerson(contact.identifier)"></span>
                </li>
            </ul>
        </div>
        <div class="rbtn"></div>
    </div>
    <div class="ft">
        <input ng-if="groupPersons.length==0" type="button" value="确定" ng-click="closeAddContactBox()">
        <input ng-if="groupPersons.length>0" type="button" value="确定({{groupPersons.length}})" ng-click="saveGroupContact();">
    </div>
</div>