<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="listContainer">
    <audio id="msgaudio"  controls="controls"  hidden="true">
            <source src="${pageContext.request.contextPath}/sound/msg.mp3" type="audio/ogg">
            <source src="${pageContext.request.contextPath}/sound/msg.wav" type="audio/mpeg">
                                您的浏览器不支持 audio 元素。
    </audio>

	<!-- 个人信息及操作的容器 -->
	<div class="profile">
		<h3 class="profileName">{{user.name}}</h3>
		<span class="iconOperater" ng-click="changeOperaterListStatu($event);"></span>
		<div class="myProfile" ng-click="updateAvatar($event);">
			<img ng-if="user.avatar" ng-src="{{user.avatar|editAvatarFormat}}">
			<img ng-if="!user.avatar" src="${pageContext.request.contextPath}/images/default_avatar40.png">
		</div>
		<div class="operaterBox" tabindex="-1" id="operaterBox" ng-show="showOperaterList" style="height:154px">
			<!-- <operator-items></operator-items> -->
			<div class="operaterBoxPanel">
			    <a href="javascript:void(0);" class="addFriends" ng-click="clickCreateGroupBut($event);">
			        <span class="iconPic fl" style="background: url(${pageContext.request.contextPath}/images/icon/addchat2.png) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">发起聊天</span>
			        <div class="clr"></div>
			    </a>
			    <div id="showMute" style="display:none">
			    <a href="javascript:void(0);" class="voiceCancel" ng-click="changeNotification($event);" ng-show="!notification">
			        <span class="iconPic  fl" style="background: url(${pageContext.request.contextPath}/images/icon/close_mute.gif) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">桌面通知</span>
			        <div class="clr"></div>
			    </a>
			    <a href="javascript:void(0);" class="voiceCancel" ng-click="changeNotification($event);" ng-show="notification">
			        <span class="iconPic  fl" style="background: url(${pageContext.request.contextPath}/images/icon/open_mute.gif) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">关闭通知</span>
			        <div class="clr"></div>
			    </a>
			    </div>
			    <a href="javascript:void(0);" class="voiceCancel" ng-click="changeVoiceStatu($event);" ng-if="!voiceStatu">
			        <span class="iconPic  fl" style="background: url(${pageContext.request.contextPath}/images/icon/voicecancel3.png) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">开启声音</span>
			        <div class="clr"></div>
			    </a>
			    <a href="javascript:void(0);" class="voiceCancel" ng-click="changeVoiceStatu($event);" ng-if="voiceStatu">
			        <span class="iconPic  fl" style="background: url(${pageContext.request.contextPath}/images/icon/voicecancel2.png) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">关闭声音</span>
			        <div class="clr"></div>
			    </a>
			    <a href="javascript:void(0);" class="feedback" ng-click="doFeedBack($event);">
			        <span class="iconPic fl" style="background: url(${pageContext.request.contextPath}/images/icon/feedback2.png) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">意见反馈</span>
			        <div class="clr"></div>
			    </a>
			    <a href="javascript:void(0);" class="iconLogout" ng-click="doQuitBack($event);">
			        <span class="iconPic fl" style="background: url(${pageContext.request.contextPath}/images/icon/Logout2.png) no-repeat;"></span>
			        <span class="operaterBoxTitle fl">退出</span>
			        <div class="clr"></div>
			    </a>
			</div>
		</div>
	</div>
	<!-- 中间搜索及切换区域 -->
	<div class="middleContainer">
		<div class="operatorContent">
			<div class="conmu active">
		        <span class="bag">
		            <a title="聊天" class="chat" ng-class="{cur : !showAddress}" ng-click="showAddress=false;resetChatBoxStyle(0)">
		                <span class="allmsgNum" ng-if="(allUnreadMessages>0 && allUnreadMessages<=99) && showAddress">{{allUnreadMessages}}</span>
		                <span class="allmsgNum" ng-if="(allUnreadMessages>0 && allUnreadMessages>99) && showAddress">99+</span>
		            </a>
		        </span>
		    </div>
		    <div class="cutOffRule">
		        <span class="bag">|</span>
		    </div>
		    <div class="addr">
		        <span class="bag">
		            <a title="通讯录" class="contact" ng-class="{cur : showAddress}" ng-click="showAddress=true;resetChatBoxStyle(1)"></a>
		        </span>
		    </div>
		</div>
		<div class="inputContent">
			<input type="text" class="left" placeholder="搜索：联系人、群组" ng-model="searchCondition"  ng-change="fullTextSearch()"/>
            <span class="search left"></span>
            <span class="searchClean left" ng-show="searchCondition.length>0 " ng-click="searchCondition='' "></span>
		</div>
	</div>
	<!-- 聊天列表及通讯录区域 -->
	<div class="mainListContainer" id="mainListContainer" style="overflow-y:auto;overflow-x:hidden">
		<!-- <div class="chatContainer" ng-hide="showAddress"> -->
		<div class="{{chatContainerStyle}}" ng-hide="showAddress">
            <div class="scroller" ng-show="searchCondition == null || searchCondition.length == 0 ">

                <div ng-class="{true:'left_chat',false:'chatListColumn'}[showMessageBox&&chat.identifier==currentChatObj.identifier]" ng-repeat="(key,chat) in chatsMap | toArray | orderBy:'operaterTime':true " ng-click="recentChat(chat, $event);">
                    <div class="avatar_wrap" style="margin-right: 10;float: left;">
                        <span class="avatar">
                            <img ng-if="chat.avatar" ng-src="{{chat.avatar|editAvatarFormat}}" alt="{{chat.name}}">
                            <img ng-if="!chat.avatar && chat.isGroup" src="${pageContext.request.contextPath}/images/goup_default.png" alt="{{chat.name}}">
                            <img ng-if="!chat.avatar && !chat.isGroup" src="${pageContext.request.contextPath}/images/oneps40.png" alt="{{chat.name}}">
                         </span>
                        <span class="amsgNum" ng-if="(chat.unreadMessages>0&&chat.unreadMessages<=99)&&chat.identifier!=currentChatObj.identifier">{{chat.unreadMessages}}</span>
                        <span class="amsgNum" ng-if="(chat.unreadMessages>0&&chat.unreadMessages>99)&&chat.identifier!=currentChatObj.identifier">99+</span>
                    </div>
                    <div class="info">
                        <div class="nickName" ng-show="chat.identifier!=currentChatObj.identifier">
                            <div class="left name">
                                {{chat.name}}
                                <span style="font-size: 12px;color: #A3A3A2" ng-show="chat.userCount">({{chat.userCount|groupUserCountFormat}})</span>
                            </div>
                            <div class="desc">
                                <div class="miniSendError" ng-if="chat.sendState==2"></div>
                                <div class="sendingState" ng-if="chat.sendState==0"></div>
                                {{chat.messageContent}}
                            </div>
                            <div class="time">{{chat.messageTime | dateFormat }}</div>
                        </div>
                        <div class="nickName" ng-show="chat.identifier==currentChatObj.identifier">
                            <div class="left name"  style="color: #FFF">
                                {{chat.name}}
                                <span style="font-size: 12px;color: #FFF" ng-show="chat.userCount">({{chat.userCount}})</span>
                            </div>
                            <div class="desc" style="color: #FFF">
                                <div class="miniSendError" ng-if="chat.sendState==2"></div>
                                <div class="sendingState" ng-if="chat.sendState==0"></div>
                                {{chat.messageContent}}
                            </div>
                            <div class="time" style="color: #FFF">{{chat.messageTime | dateFormat }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  <div class="more"><span>更多</span>点击显示更多</div> -->
        </div>
		<!-- 所在部门 --><!--
		<div class="groupTitleDetails" >
       	<ul>
       		<li class="titles titles-{{!showContacts}}" >
	            {{theDept.name}}
	        	<span>({{theDept.membercount}})</span>
	        </li>
	      </ul>
 		</div>
 		-->
        <!-- 通讯录 -->
		<div class="contactContainer" ng-if="showAddress">
			<div class="scroller" ng-if="searchCondition == null || searchCondition.length == 0 ">
				<div class="friendDetailWrap">
			        <a class="groupGroup pointer" ng-click="showGroupBox=!showGroupBox">
			            <span class="avatar fl">
			                <img src="${pageContext.request.contextPath}/images/group.png">
			            </span>
			            <div class="left name">群组</div>
			            <div class="clr"></div>                                    
			        </a>
			        <div class="groupGroupDetails" ng-if="showGroupBox">
				        <a class="groupFriends departmentGroup" ng-repeat="group in groups | filterInvalid " ng-click="groupChat(group);resetChatBoxStyle(0)">
				            <span class="avatar"><img src="${pageContext.request.contextPath}/images/goup_default.png"></span>
				            <div class="left name" style="">{{group.name}}</div>
				            <div class="clr"></div>
				        </a>
				    </div>
                    <div org-tree></div>
                </div>
				<ng-include src="'${pageContext.request.contextPath}/pages/main/template/starFriends.jsp'"></ng-include>
			</div>
        </div>
        <!--查询结果-->
        <ng-include src="'${pageContext.request.contextPath}/pages/main/template/searchFullResult.jsp'"></ng-include>
    </div>
</div>