<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="contactSetupContainer">
    <div class="grouphd">
        <span class="iconOperaterleft" ng-click="hideSetup();"></span>
        <h3>聊天信息</h3>
    </div>
    <div class="bd">
        <ul class="group" style="height: 452px;">
            <li ng-if="isGroup" ng-repeat="contact in currentContacts">
            	<p class="images" ng-click="personChat(contact)" ng-mouseover="">
            		<img ng-if="contact.avatar" ng-src="{{contact.avatar|editAvatarFormat}}">
	               	<img ng-if="!contact.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
            	</p>
            	<h4>{{contact.name}}</h4>
            	<span class="del" ng-click="exitGroup(contact)"  ng-show="groupCreator == user.username && groupCreator != contact.username"></span>
            </li>
            <li ng-if="!isGroup">
            	<p class="images" ng-click="personChat(currentChatObj)">
            		<img ng-if="currentChatObj.avatar" ng-src="{{currentChatObj.avatar|editAvatarFormat}}">
	               	<img ng-if="!currentChatObj.avatar" src="${pageContext.request.contextPath}/images/oneps40.png">
            	</p>
            	<h4>{{currentChatObj.name}}</h4>
            	<span class="del" ng-if="isGroup"></span>
            </li>
            <li class="last" ng-show="!currentChatObj.isDeptGroup">
            	<img src="${pageContext.request.contextPath}/images/addimg.png" ng-click="showMaskBox = true;showAddGroupContactBox = true;"/>
            </li>
        </ul>
        <div class="modifyGroupName" ng-show="isGroup">
            <span class="spanLabel">群聊名称:&nbsp;&nbsp;</span>
            <input type="text" class="inputname fl" ng-model="groupName">
            <input type="button" value="确定" class="queding fl" ng-click="updateGroupName()" ng-show="groupCreator == user.username">
        </div>
        <div class="cl"></div>
    </div>
</div>