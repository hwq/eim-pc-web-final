<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modifyavatar" ng-show="showModifyAvatar">
	<div class="header">
		<div class="close" ng-click="showModifyAvatar=false;showMaskBox=false;showImageEdit=false;user.tempAvatar=user.avatar"></div>
	   	<h3>修改头像</h3>
	</div>
	<div class="content">
		<div class="avatorInfo" style="position:relative;height: 360px;">
			<div class="loadingMask" style="display:none;"></div>
			<div class="preAvartor" ng-show="!showImageEdit">
				<img ng-if="user.avatar" ng-src="{{user.avatar|editAvatarFormat}}">
				<img ng-if="!user.avatar" src="${pageContext.request.contextPath}/images/default_avatar40.png">
				<div class="editOper">
					<a class="btn btnBrown left" href="javascript:;">
						<input type="button" ng-click="showImageEdit=!showImageEdit;editAvatar()" value="编辑">
					</a>
				</div>
			</div>
			<div class="editAvartor" ng-show="showImageEdit">
				<div style="padding-left: 30px;padding-top: 30px;">
					<div class="bigPreAvartor">
						<img ng-if="user.tempAvatar" ng-src="{{user.tempAvatar|editAvatarFormat}}"  height="244px" width="244px">
						<img ng-if="!user.tempAvatar" src="${pageContext.request.contextPath}/images/default_avatar40.png" height="244px" width="244px"/>
					</div>
					<div class="smallPreAvartor">
						<img ng-if="user.tempAvatar" ng-src="{{user.tempAvatar|editAvatarFormat}}"  height="60px" width="60px">
						<img ng-if="!user.tempAvatar" src="${pageContext.request.contextPath}/images/default_avatar40.png" width="60px" height="60px"/>
                 		<input type="button" value="上传图片" id="btnUploadUserAvatar" >
                 		<input type="file" id="uploadUserAvatar" value="上传图片" accept="image/*" style="display: none">
             		</div>
				</div>
				<div class="inputBox" style="margin-top: 26px;">              
			        <input type="button" ng-if="!showLoad" value="保存" ng-click="updateUserAvatar()">
                    <input class="btn" ng-if="showLoad" type="submit" value="保存中.." ng-disabled='1'/>
			        <input type="button" value="取消" ng-click="showImageEdit=!showImageEdit;showLoad=false">
			    </div>
			</div>
		</div>
	</div>
</div> 