<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10">
    <meta name="keyworks" content="@work网页版,@work,@work web,@wrok pc,@work电脑版,@work电脑,@work pc web">
    <title>@work网页版</title> 
<%-- <script type="text/javascript"
	src="${pageContext.request.contextPath}/js/import.js"></script> --%>
	<jsp:include page="/pages/common/header.jsp"></jsp:include>
    <jsp:include page="/pages/main/template.jsp"></jsp:include>
</head>

<body ng-controller="bodyController" ng-click="showOperaterList = showOperaterList?false:showOperaterList" ng-keyup="shortKeySendMessage($event)" ng-cloak>
	<center>
		<!-- 背景遮蔽层 -->
		<div id="mask" class="mask" ng-show="showMaskBox"></div>
		<!-- 修改图像层开始 -->
		<jsp:include page="/pages/main/modify_avatar.jsp"/> 
		<!-- 修改图像层结束 -->
		<!-- 退出层 -->
	    <div class="quitBox" ng-show="showQuitBox">
	        <div class="hd">
	            <h4 class="fl">退出提示</h4>
	        </div>
	        <div class="bd">您确定要退出@work网页版吗?</div>
	        <div class="ft">
	            <input type="button" value="确定" class="logOut" ng-click="doLogout($event);">
	            <input type="button" value="取消" class="closebox" ng-click="showQuitBox=false;showMaskBox=false;">
	        </div>
	    </div>
	    <!-- 消息反馈层 -->
	    <div class="feedbackbox" ng-show="showFeedbackBox">
		    <div class="hd">
		        <h4 class="fl">意见反馈</h4>
		        <span class="closebox" ng-click="closeFeedBack($event);"></span>
		    </div>
		    <iframe name="feedbackIframe" src="${feedbackAddress}" frameborder="0" width="100%" height="200px"></iframe>
		</div>
		<!-- 点击聊天个人信息确认层开始 -->
		<jsp:include page="/pages/main/chat_confirm.jsp"/> 
	    <!-- 点击聊天个人信息确认层结束 -->
	    <!-- 添加群成员列表开始 -->
	    <jsp:include page="/pages/main/add_contact.jsp"/> 
		<!-- 添加群成员列表结束 -->
		<div class="container">
			<center>
				<div class="contentContainer">
					<!-- 左边列表的容器开始 -->
					<jsp:include page="/pages/main/left_box.jsp"/>
					<!-- 左边列表的容器结束 -->
					<!-- 中间空条框的开始 -->
					<div class="vernierContainer">
						<div class="associatedWrap">
		                    <img src="${pageContext.request.contextPath}/images/associated.png" class="associated">
		                </div>
					</div>
					<!-- 中间空条框的结束 -->
					<!-- 右边窗口的欢迎语 -->
					<div class="welcomeBox" style="height: 548.52px;">
		                <img src="${pageContext.request.contextPath}/images/welcome.png" alt="欢迎来到@work网页版" title="欢迎来到@work网页版">
		            </div>
					<!-- 右边聊天窗口的开始 -->
					<jsp:include page="/pages/main/right_message.jsp"/>
					<!-- 右边聊天窗口的结束 -->
					<!-- 聊天人员设置页面开始 -->
					<jsp:include page="/pages/main/right_setup.jsp"/>
					<!-- 聊天人员设置页面结束 -->
                    <!-- 群聊添加人员 -->
                    <jsp:include page="/pages/main/add_group_contact.jsp"></jsp:include>
                </div>
			</center>
		</div>
		<!-- 底部页脚区域 -->
		<jsp:include page="/pages/common/footer.jsp"/> 
	</center>
</body>
</html>
