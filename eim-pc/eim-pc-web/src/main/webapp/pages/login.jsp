<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keyworks" content="@work网页版,@work,@work web,@wrok pc,@work电脑版,@work电脑,@work pc web">
	<meta http-equiv="X-UA-Compatible" content="IE=10">
    <!-- <meta name="renderer" content="Blink"> -->
    <title>@work网页版</title> 
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/app.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/login.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/sweet-alert.css">
    <script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery/jquery-1.11.1.js"></script>
    <script src="${pageContext.request.contextPath}/js/angular/1.3.0/angular.js"></script>
    <script src="${pageContext.request.contextPath}/js/sweetalert-0.3.3/sweet-alert.js"></script>
</head>

<body ng-app="loginApp">
    <div class="header">
        <div class="logo fl">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="@work企业级通讯办公平台">
        </div>
        <div class="logoTip fl">
            <p class="nth1">@wrok</p>
            <p class="nth2">企业级通讯办公平台</p>
        </div>
        <div class="cl"></div>
    </div>
    <div class="content">
        <div class="webimg fl">
            <img src="${pageContext.request.contextPath}/images/web.png" alt="@wrok网页版">
        </div>
        <div class="loginbox fr" ng-controller="loginController">
            <form name="loginForm" ng-submit="login(loginForm.$valid)" novalidate>
                <div class="hd">
                    <h3>登录</h3>
                </div>
                <p class="errorTip" ng-bind="errorTip">
                </p>
                <div class="bd">
                    <p class="usernamebox">
                        <label>用户名 : &nbsp;</label>
                        <input type="text" class="input" name="username" ng-model="user.username" placeholder="请输入用户名" autofocus/>
                    </p>
                    <p class="pswbox">
                        <label>密&nbsp;&nbsp;&nbsp;码 : &nbsp;</label>
                        <input type="password" class="input" ng-model="user.password" placeholder="请输入密码" />
                    </p>
                    <p class="pswbox" ng-show="verifications >= 3">
                        <label>验证码 : &nbsp;</label>
                        <input type="text" class="input" ng-model="codeInput" placeholder="请输入验证码" style="width: 100px" maxlength="4" />
                        <input type="button" class="codeBtn" ng-model="codeBtn" id="codeBtn" ng-click="createCode()" title="点击刷新"/>
                        <input class="refreshBtn" ng-click="createCode()"/>
                    </p>
                </div>
                <p style="height:8px;text-align:center;display:none">
                    <input type="checkbox" name="remember">
                </p>
                <p class="ft">
                    <input class="btn" ng-if="!showLoad" type="submit" value="登录" ng-click="showLoad = true" style="background:url(${pageContext.request.contextPath}/images/sendmsg_btn_bg.png) repeat-x"/>
                     <input class="btn" ng-if="showLoad" type="submit" value="登录中.." ng-disabled='1'/>
                    <!--
                    <div id="caseVerteClaire" ng-if="showLoad">
    					<div id="transform">
        				<div id="transform1"></div>
        				<div id="transform2"></div>
       					<div id="transform3"></div>
   					 	</div>
   					 </div>-->
                </p>
            </form>
        </div>
    </div>
    <jsp:include page="/pages/common/footer.jsp"/> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/login.js"></script>
</body>
</html>
