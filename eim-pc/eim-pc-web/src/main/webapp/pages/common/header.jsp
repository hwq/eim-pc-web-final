<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setBundle basename="systemConfig"/><fmt:message key="ts" var="ts"/>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/other/console.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/other/uuid.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/other/moment.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/util.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/websocket-util.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/tool.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/sweetalert-0.3.3/sweet-alert.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery/jquery-1.11.1.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/angular/1.3.0/angular.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/angular/loading-bar.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/app.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/cache-services.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/directives.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/filters.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/im-services.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/managers.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/stores.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/view/message-view.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/view/data-view.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/model/models.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/handler/message-handlers.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/handler/chat-handler.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/handler/group-handler.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/handler/contact-handler.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/handler/file-handler.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/handler/organization-handler.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/core/controller/controllers.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/expEmoji.js?${ts}'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/playAudio.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/lightbox/lightbox-2.6.min.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/js/jquery/jquery.textsearch.js'></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/app.css?${ts}">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css?${ts}">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/emoji.css?${ts}">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/emoji_panel.css?${ts}">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/loading-bar.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/sweet-alert.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/lightbox.css" media="screen">

<script type="text/javascript">
    window.rootPath = '${pageContext.request.contextPath}';
</script>
