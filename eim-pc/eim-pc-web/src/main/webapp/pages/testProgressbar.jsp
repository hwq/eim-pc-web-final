
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery/jquery-1.11.1.js"></script>
<script
	src="${pageContext.request.contextPath}/js/angular/1.3.0/angular.js"></script>

<SCRIPT type=text/javascript>
	function doProgressLoop(prog, max, counter) {
		var x = document.getElementById('progress-content').innerHTML;
		var y = parseInt(x);
		if (!isNaN(y)) {
			prog = y;
		}
		counter = counter + 1;

		console.info("prog="+prog);
		if (prog < 100) { 
			setTimeout("getProgress()", 1000);
			setTimeout("doProgressLoop(" + prog + "," + max + "," + counter
					+ ")", 1500);
			document.getElementById('progressBarText').innerHTML = 'upload in progress: '
					+ prog + '%';
			document.getElementById('progressBarBoxContent').style.width = parseInt(prog)
					+ '%';

		}else{
			document.getElementById('progressBarText').innerHTML = 'upload in progress: '
                + prog + '%';
			document.getElementById('progressBarBoxContent').style.width = parseInt(prog)
            + '%';
		}
	}

	function getProgress() {
		$.ajax({ url: "${pageContext.request.contextPath}/fileOperations/getProgress.do", context: "", success: function(data){
             console.info("data.progress="+data.progressvo.prog);
			 document.getElementById('progress-content').innerHTML=data.progressvo.prog;
	      }}); 
	}

	function fSubmit() {
		var button = window.document.getElementById("submitButton");
		button.disabled = true;
		var max = 100;
		var prog = 0;
		var counter = 0;
		document.getElementById('progressBar').style.display = 'block';
		document.getElementById('progressBarText').innerHTML = 'upload in progress: 0%';
		getProgress();
		doProgressLoop(prog, max, counter);
		document.getElementById("form1").submit();
	}
</SCRIPT>
<iframe name="ajaxUpload" style="display:none"></iframe>
<form name="form1" id="form1" target="ajaxUpload" action="${pageContext.request.contextPath}/fileOperations/upload.do" method="post"
	enctype="multipart/form-data">

	<span>选择文件</span>
	<div style="padding-bottom: 5px;">
		<input type="file" size="48" name="files" id='filedata'>
	</div>

	<div id="progressBar" style="display: none;">

		<div id="theMeter">
			<div id="progressBarText"></div>

			<div id="progressBarBox"
				style="color: Silver; border-width: 1px; border-style: Solid; width: 300px; TEXT-ALIGN: left">
				<div id="progressBarBoxContent"
					style="background-color: #3366FF; height: 15px; width: 0%; TEXT-ALIGN: left"></div>
			</div>
			<div id="progress-content" style="display: none;"></div>
		</div>
	</div>
	<INPUT id='submitButton' tabIndex=3 type=button value="提交"
		onclick='fSubmit();'>
</form>
