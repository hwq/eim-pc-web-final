(function (exports, window) {
	var tool = exports;
	tool.extend = function (subClass,superClass){
	    var F = function(){};
	    F.prototype = superClass.prototype;
	    subClass.prototype = new F();
	    subClass.prototype.constructor = subClass;
	    subClass.superclass = superClass.prototype; //加多了个属性指向父类本身以便调用父类函数
	    if(superClass.prototype.constructor == Object.prototype.constructor){
	        superClass.prototype.constructor = superClass;
	    }
	};
	
	tool.timeFormat = function(time, isShowBeforeToday){
		time = moment(time);
		if(time.isValid()){
			if(moment().diff(time, 'days')>0){
				if(isShowBeforeToday) return "";
				return time.format("YYYY/M/D HH:mm");
			}else{
				return time.format("HH:mm");
			}
		}else{
			return time + "is invalid!";
		}
	};
	
	
	//对Array做一个扩展
	Array.prototype.clear=function(){ 
		this.length=0; 
	} 
	Array.prototype.insertAt=function(index,obj){ 
		this.splice(index,0,obj); 
	} 
	Array.prototype.removeAt=function(index){ 
		this.splice(index,1); 
	} 
	Array.prototype.remove=function(obj){ 
			var index=this.indexOf(obj); 
			if (index>=0){ 
			this.removeAt(index); 
		} 
	}
	Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
	    return false;
	};

})(this.tool ={}, this);