'use strict';

/* Services */

angular.module('myApp.MessageModel', []).
	factory("Message", ['$rootScope',function ($rootScope) {
		var uuid = window.uuid;
		var _ = window._;
		var moment = window.moment;
		
		//父类message
		function Message(){
			this.id = uuid.v4();
		}

		//聊天消息的父类
		function ChatMessage(from,to,avatar){
			Message.call(this);
			this.from=from;
            this.fromName = null;
			this.to = to;
            this.avatar = avatar;
			this.timestamp = util.getServerTime();
		}
		tool.extend(ChatMessage, Message);
		
		//文本消息
		function TextMessage(from,to,avatar,content){
			ChatMessage.call(this,from,to,avatar);
			this.msgType=11;
			this.content = content;
		}

        //图片消息
		function ImageMessage(from,to,avatar,content,mediaId){
			ChatMessage.call(this,from,to,avatar);
			this.msgType=12;
			this.mediaId = mediaId;
			this.thumbContent = content;
			this.sendstats = 0;
		}

        //文件消息
        function FileMessage(from,to,avatar,content,mediaId,filename,filesize){
            ChatMessage.call(this,from,to,avatar);
            this.msgType=18;
            this.mediaId = mediaId;
            this.thumbContent = content;
            this.filename = filename;
            this.filesize=filesize;
        }
		
		//心跳消息
		function BeatHeartMessage(){
			Message.call(this);
		}
		//请求认证消息
		function RequestMessage(token){
			Message.call(this);
			this.requestType = 31;
			this.token = token;
		}
		//响应消息
		
		//命令消息
		function CommandMessage(type){
			Message.call(this);
			this.commType =type;
		}
		tool.extend(CommandMessage, Message);
		
		//退出命令消息
		function QuitMessage(){
			CommandMessage.call(this,51);
		}


        //回执消息
        function ReceiptMessage(id){
            this.msgType = 41;//message type
            this.code = 100;//message code
            this.id = id;//target message id
        }
        function extend(subClass, superClass){
            tool.extend(subClass, superClass);
            return subClass;
        }
		
		return {
			BeatHeartMessage : extend(BeatHeartMessage,Message),
			RequestMessage : extend(RequestMessage,Message),
			TextMessage : extend(TextMessage,ChatMessage),
			ImageMessage : extend(ImageMessage,ChatMessage),
            FileMessage : extend(FileMessage, ChatMessage),
			QuitMessage : extend(QuitMessage,CommandMessage),
            ReceiptMessage : ReceiptMessage
		};
	}]);

angular.module('myApp.ChatModel', [])
    .factory("Chat", function () {
        function Chat() {
            this.identifier = null;//用户名或群ID
            this.isGroup = false;//是否为群聊
            this.userCount = null;//群聊人数
            this.name = null;//用户名称或群名
            this.avatar = null; //头像
            this.msgType = null;//消息类型 11:文本， 12：图片， 18：文件
            this.messageContent = null;//内容
            this.messageTime = null;//消息时间
            this.isme=null;//保存用户名
            this.sendState=1;//发送状态  0.发送中  1.成功 2.失败 
            this.unreadMessages = 0;//未读消息
            this.operaterTime = util.getServerTime();//操作时间
        }

        //群对象转聊天对象
        function groupToChat(group){
            var chat = new Chat();
            chat.isGroup = true;
            chat.identifier = group.identifier;
            chat.name = group.name;
            chat.avatar = group.avatar;
            chat.deptId = group.deptId;
            chat.userCount = group.userCount;
            return chat;
        }

        //联系人对象转聊天对象
        function contactToChat(contact){
            var chat = new Chat();
            chat.isGroup = false;
            chat.identifier = contact.identifier;
            chat.name = contact.name;
            chat.avatar = contact.avatar;
            return chat;

        }

        return {
            build: function () {
                return new Chat();
            },
            groupToChat : groupToChat,
            contactToChat : contactToChat
        }
    });


angular.module('myApp.GroupModel', [])
    .factory("Group", function () {
        function Group() {
            this.identifier = null;//群ID
            this.name = null;//名称
            this.avatar = null; //头像
            this.deptId = null; //所属部门ID
            this.userCount = 0;//人员数量
        }


        return {
            build: function () {
                return new Group();
            }
        }
    });

angular.module('myApp.ContactModel', [])
    .factory("Contact", function () {
        function Contact() {
            this.identifier = null;//ID
            this.name = null;//名称
            this.avatar = null;//头像
            this.tel = null;//电话
            this.email = null;//邮箱
        }

        return {
            build: function () {
                return new Contact();
            }
        }
    });