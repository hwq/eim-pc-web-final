'use strict';

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.CacheServices', []).
	factory("LocalCacheService", ['$rootScope','$location',function ($rootScope,$location) {
		var localCache = window.localStorage;
		var prefix = function(){
			if($rootScope.user==null){
				return "";
			}
			return ($location.host() + "_" + $rootScope.user.identifier + "_");
		};
		return {
			get: function (key) {
				key = prefix() + key;
				return localCache.getItem(key);
			},
			set: function (key, val) {
				key = prefix() + key;
				return localCache.setItem(key, val);
			},
			del: function (key) {
				key = prefix() + key;
				return localCache.removeItem(key);
			},
			clear: function(){
				localCache.clear();
			}
		};
	}]).factory("SessionCacheService", ['$rootScope',function ($rootScope) {
		var sessionCache = window.localStorage;
		return {
			get: function (key) {
				return sessionCache.getItem(key);
			},
			set: function (key, val) {
				return sessionCache.setItem(key, val);
			},
			del: function (key) {
				return sessionCache.removeItem(key);
			},
			clear: function(){
				sessionCache.clear();
			}
		};
	}]);


