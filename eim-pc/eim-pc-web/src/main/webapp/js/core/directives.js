'use strict';

/* Directives */

angular.module('myApp.directives', ['myApp.CacheServices','myApp.DataStore']).
    directive('organization', ['DataStore', function (DataStore) {
    	return {
			restrict: 'E',
			replace: true,
			transclude: true,
			scope: {organization:'=',groups:'='},
			template: '<div ng-transclude></div>',
			controller: function ($scope) {
				$scope.groups=DataStore.getGroups($scope.organization['identifier']);
			}
    	};
    }]).
    directive('orgTree', ['DataStore', function (DataStore) {
    	return {
			restrict: 'A',
			replace: true,
			transclude: true,
            templateUrl :  window.rootPath + '/pages/main/template/orgTree.jsp',
            controller: function ($rootScope, $scope, $compile, $interval) {
                var promise = $interval(function(){
                    $scope.organizations = DataStore.getOrganizations();
                    $scope.contacts = DataStore.getContacts();
                    if($scope.organizations.length > 0){
                        $interval.cancel(promise);
                    }
                }, 500);

                var loaded = [];
                $scope.getChildren = function ($event) {
                    var parentElem = $event.target.parentElement;
                    var deptId = angular.element(parentElem).scope().deptId;
                    if(loaded.contains(deptId)){
                        return;
                    }else{
                        loaded.push(deptId);
                    }
                    if (parentElem.nodeName == 'LI') {
                        parentElem = parentElem.parentElement;
                    }
                    $.ajax({
                        url: window.rootPath + '/pages/main/template/deptDetail.jsp',
                        dataType: 'html',
                        async : false,
                        success: function (html) {
                            var element = $compile(html)(angular.element(parentElem).scope());
                            angular.element(parentElem).append(element);
                        }
                    });
                }
			}
    	};
    }]).
    directive('addContactOrgTree', ['DataStore', function (DataStore) {
        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            templateUrl :  window.rootPath + '/pages/main/template/addContactOrgTree.jsp',
            controller: function ($rootScope, $scope, $compile, $interval) {
                var promise = $interval(function(){
                    $scope.organizations = DataStore.getOrganizations();
                    $scope.contacts = DataStore.getContacts();
                    if($scope.organizations.length > 0){
                        $interval.cancel(promise);
                    }
                }, 500);

                var loaded = [];
                $scope.getChildren = function ($event, type) {
                    var parentElem = $event.target.parentElement;
                    var deptId = angular.element(parentElem).scope().deptId;
                    if(loaded.contains(deptId)){
                        return;
                    }else{
                        loaded.push(deptId);
                    }
                    if (parentElem.nodeName == 'LI') {
                        parentElem = parentElem.parentElement;
                    }
                    var url = '/pages/main/template/addContactDeptDetail.jsp';
                    if(type == 'addGroupContact'){
                        url = '/pages/main/template/addGroupContactDeptDetail.jsp';
                    }
                    $.ajax({
                        url: window.rootPath + url,
                        dataType: 'html',
                        async : false,
                        success: function (html) {
                            var element = $compile(html)(angular.element(parentElem).scope());
                            angular.element(parentElem).append(element);
                        }
                    });
                }
            }
        };
    }]).
    directive('addGroupContactOrgTree', ['DataStore', function (DataStore) {
        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            templateUrl :  window.rootPath + '/pages/main/template/addGroupContactOrgTree.jsp',
            controller: function ($rootScope, $scope, $compile, $interval) {
                var promise = $interval(function(){
                    $scope.organizations = DataStore.getOrganizations();
                    $scope.contacts = DataStore.getContacts();
                    if($scope.organizations.length > 0){
                        $interval.cancel(promise);
                    }
                }, 500);

                var loaded = [];
                $scope.getChildren = function ($event) {
                    var parentElem = $event.target.parentElement;
                    var deptId = angular.element(parentElem).scope().deptId;
                    if(loaded.contains(deptId)){
                        return;
                    }else{
                        loaded.push(deptId);
                    }
                    if (parentElem.nodeName == 'LI') {
                        parentElem = parentElem.parentElement;
                    }
                    $.ajax({
                        url: window.rootPath + '/pages/main/template/addGroupContactDeptDetail.jsp',
                        dataType: 'html',
                        async : false,
                        success: function (html) {
                            var element = $compile(html)(angular.element(parentElem).scope());
                            angular.element(parentElem).append(element);
                        }
                    });
                }
            }
        };
    }]).
    directive('onMessageFinishRender', ['$timeout',function ($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function() {
                        scope.$emit('ngMessageRepeatFinished');
                    });
                }
            }
        };
    }]).
    directive('operatorItems', ['LocalCacheService', function (LocalCacheService) {
    	return {
			restrict: 'E',
			replace: true,
			transclude: true,
			scope: {},
			template: ['<div class="operaterBoxPanel">',
			           		'<a href="javascript:void(0);" ng-repeat="item in items" class="{{item.aclass}}" ng-click="doOperater(item)">',
			           			'<span class="iconPic fl" style="background: url({{item.bgUrl}}) no-repeat;"></span>',
			           			'<span class="operaterBoxTitle fl">{{item.text}}</span>',
			           			'<div class="clr"></div>',
			           		'</a>',
			           	'</div>'
			           ].join(""),
			link: function (scope, element, attrs) {
				
			},
			controller: function ($scope) {
				$scope.voiceStatu = LocalCacheService.get("voiceStatu")&&LocalCacheService.get("voiceStatu")==1?true:false;
				$scope.items = [{id:1,aclass:"addFriends",bgUrl:"/images/icon/addchat2.png",text:"发起聊天"},
				                {id:2,aclass:"voiceCancel",bgUrl:$scope.voiceStatu?"/images/icon/voicecancel3.png":"/images/icon/voicecancel2.png",text:"开启声音"},
				                {id:3,aclass:"feedback",bgUrl:"/images/icon/feedback2.png",text:"意见反馈"},
				                {id:4,aclass:"iconLogout",bgUrl:"/images/icon/Logout2.png",text:"退出"}];
				$scope.doOperater = function(item){
					if(item.id==2){
						if($scope.voiceStatu){
							item.bgUrl = "/images/icon/voicecancel2.png";
							LocalCacheService.del("voiceStatu");
						}else{
							item.bgUrl = "/images/icon/voicecancel3.png";
							LocalCacheService.set("voiceStatu", 1);
						}
						$scope.voiceStatu = !$scope.voiceStatu;
					}
				};
			}
		};
    }]).directive('focus', function () {
        return function (scope, element, attrs) {
            element.focus();
        }
    }).directive('messageTime', function(){
        return {
            restrict : 'EA',
            replace : true,
            scope : {
                timestamp : '@'
            },
            template : '<div class="time"><span class="timeBg"></span>{{timestamp | dateFormat}}<span class="timeBg"></span></div>',
            controller :
                function($rootScope, $scope, $element, $transclude){
                }
        }
    });
