// Declare app level module which depends on filters, and services
//'myApp.DataStore','myApp.DataView',
angular.module('myApp', ['myApp.DataView','myApp.IMServices','myApp.controllers','myApp.directives','myApp.IMServices', 'myApp.filters','chieffancypants.loadingBar']).
value("Constant",{
	authenicated:"authenicated"
}).run(['DataView','IMService', function (DataView,IMService) {
    $(function () {
        //阻止浏览器默认行。
        $(document).on({
            dragleave: function (e) {    //拖离
                e.preventDefault();
            },
            drop: function (e) {  //拖后放
                e.preventDefault();
            },
            dragenter: function (e) {    //拖进
                e.preventDefault();
            },
            dragover: function (e) {    //拖来拖去
               e.preventDefault();
            }
        });
    });
	//整体高度自适应
	util.changeHeight();
	$(window).resize(util.changeHeight);
	//初始化
    DataView.initPromise().then(function (user) {
        //链接IM
        var option = {
            domain: user.host,
            port: 10001,
            path: '/websocket',
            sessionToken: user.sessionToken
        };
        IMService.init(option);
    }, function (errorMsg) {
        swal(errorMsg);
        window.location.href = 'login.do';
    });
    IMService.checkSendState();
}]);

