'use strict';
/**
 * Created by yanyuan on 2015/1/20.
 */
angular.module('myApp.OrganizationHandler', ['myApp.DataStore'])
    .factory('OrganizationHandler', ['$rootScope','DataStore', function($rootScope, DataStore){

        var saveOrganization = function(org){
            DataStore.saveOrganization(org);
        }

        var getOrganizations = function(){
            return DataStore.getOrganizations();
        }

        var getOrganization = function(identifier){
            var org = {};
            angular.forEach(DataStore.getOrganizations(), function(organization){
                if(organization.identifier == identifier){
                    org = organization;
                }
            }) ;
            return org;
        }

        var refreshOrganization = function(){
            $rootScope.Organizations = DataStore.getOrganizations();
        }

        return{
            saveOrganization : saveOrganization,
            getOrganization : getOrganization,
            getOrganizations : getOrganizations,
            refreshOrganization : refreshOrganization
        }
}]);