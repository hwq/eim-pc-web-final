'use strict';

/* Services */

angular.module('myApp.MessageHandler', ['myApp.IMServices','myApp.Manager']).
	factory("MessageHandler", ['$rootScope','MessageView','Message','IMService','MessageManager',
        function ($rootScope,MessageView,Message,IMService,MessageManager) {
            function showFile(message) {
                var msg = MessageManager[message.msgType].copyMsg(message);
                msg.sendState = 0;
                MessageManager.saveMessage(msg);
                MessageView.update(msg);
                return message;
            }
            function showImage(message,s_filename) {
                var msg = MessageManager[message.msgType].copyMsg(message);
                msg.sendState = 0;
                MessageManager.saveMessage(msg);
                MessageView.update(msg);
                return message;
            }

            function sendFile(message) {
                var msg = MessageManager[message.msgType].copyMsg(message);
                $rootScope.messages = util.countMessage3($rootScope.messages, msg);
                msg.msgType = message.msgType;//传值视图判断
                message = MessageManager[message.msgType].send(message);
                IMService.send(message);
                return msg;
            }
            function sendImage(message) {
                var msg = MessageManager[message.msgType].copyMsg(message);
                $rootScope.messages = util.serchImageMsg($rootScope.messages, msg);
                msg.msgType = message.msgType;//传值视图判断
                message = MessageManager[message.msgType].send(message);
                IMService.send(message);
                return msg;
            }
            
            function send(message) {
                var msg = MessageManager[message.msgType].copyMsg(message);
                MessageManager.saveMessage(msg);
                msg.msgType = message.msgType;//传值视图判断
                MessageView.update(msg);
                message = MessageManager[message.msgType].send(message);
                IMService.send(message);
                return msg;
            }
            function resend(message){
            	var message2 = MessageManager[message.msgType].copyMsg(message);            	
            	var msg = MessageManager[message2.msgType].send(message2);
            	IMService.send(msg);
            	return;
            }

            return {
                text: function (from, to, avatar, content) {
                    return send(new Message.TextMessage(from, to, avatar, content));
                },
                resend: function(message){
                	resend(message);
                },
                showFile: function (from, to, avatar, content, mediaId, filename, filesize) {
                    var message = new Message.FileMessage(from, to, avatar, content, mediaId, filename, filesize);
                    return showFile(message);
                },
                sendFile: function (message) {
                    return sendFile(message);
                },
                showImage:function(from, to, avatar, content, mediaId){
                	 var message = new Message.ImageMessage(from, to, avatar, content, mediaId);
                	 return showImage(message);
                },
                sendImage:function(message){
                	return sendImage(message);
                }
            };

        }]);


