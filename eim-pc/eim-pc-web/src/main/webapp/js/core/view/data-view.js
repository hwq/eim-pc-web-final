/**
 * Created by yanyuan on 2015/1/14.
 */
'use strict';

angular.module('myApp.DataView', ['myApp.DataStore', 'myApp.CacheServices', 'myApp.GroupHandler', 'myApp.ContactHandler','myApp.ChatHandler']).
    factory("DataView", ['$rootScope', 'DataStore', 'LocalCacheService', 'GroupHandler', 'ContactHandler','ChatHandler', '$q', '$timeout',
        function ($rootScope, DataStore, LocalCacheService, GroupHandler, ContactHandler,ChatHandler, $q, $timeout) {
            $rootScope.groupMap = GroupHandler.getGroupMap();
            $rootScope.groups = GroupHandler.getGroups();

            function initViewPromise() {
                var defered = $q.defer();
                DataStore.initPromise().then(function (user) {
                    $rootScope.user = DataStore.getUser();
                    $rootScope.organizations = DataStore.getOrganizations();
                    $rootScope.contacts = ContactHandler.getContacts();
                    $rootScope.groups = GroupHandler.getGroups();
                    $rootScope.usernames = [];//初始化星标联系人姓名，过滤会用到
                    $rootScope.starContacts = {};//初始化星标联系人数组
                    ContactHandler.queryCommonlyUsedContacterPromise().then(function (result) {
                        angular.forEach(result.commonlyUsedContacters, function (commonlyUsedContacter) {
                            if(commonlyUsedContacter.status == 1){//过滤无效星标联系人
                                $rootScope.usernames.push(commonlyUsedContacter.username);
                            }
                        });
                        angular.forEach($rootScope.usernames, function (identifier) {
                            if (ContactHandler.getContact(identifier) && ContactHandler.getContact(identifier).identifier) {
                                $rootScope.starContacts[identifier] = ContactHandler.getContact(identifier);
                            }
                        });
                    }, function (errorMsg) {
                        swal({title: errorMsg, timer: 2000});
                    });
                    var groupIds = (LocalCacheService.get("groups") == null ? "" : LocalCacheService.get("groups")).split(",");
                    angular.forEach(groupIds, function (identifier) {
                        if (identifier == null || identifier == '' || GroupHandler.groupIsExist(identifier)) {
                            return;
                        }
                        GroupHandler.queryGroupPromise({"groupUid": identifier}).then(function (group) {
                            if (group == null || group.identifier == null) {
                                return;
                            }
                            GroupHandler.saveGroup(group);
                            $rootScope.groups = GroupHandler.getGroups();
                            $rootScope.groupMap = GroupHandler.getGroupMap();
                        }, function (errorMsg) {
                            swal(errorMsg);
                        })
                    });
                    $rootScope.showMaskBox = false;
                    ChatHandler.init();//初始化聊天面板
                    //初始化声音,桌面通知
                    $rootScope.voiceStatu = LocalCacheService.get("voiceStatu")&&LocalCacheService.get("voiceStatu")==1?true:false;
                    $rootScope.notification = LocalCacheService.get("notification")&&LocalCacheService.get("notification")==1?true:false;
                    defered.resolve(user);
//                $timeout(initView, 1000*60);//每隔60秒更新组织机构信息
                }, function (errorMsg) {
                    defered.reject(errorMsg);
                });
                return defered.promise;
            }

            return {
                initPromise: initViewPromise
            };
        }]);