<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="prefix" required="true" type="java.lang.String"%>
<%@ attribute name="updateGroup" required="false" type="java.lang.Boolean"%>
<organization ng-repeat="organization in organizations" organization="organization" groups="groups">
	<a class="groupTitle pointer" ng-click="showGroups=!showGroups">
		<span class="avatar fl"><img src="/images/department.jpg"></span>
       	<div class="left name" style="">{{organization.name}}</div>
       	<div class="clr"></div> 
 	</a>
	<div class="groupTitleDetails" ng-show="showGroups">
       	<group ng-repeat="group in groups" group="group"  contacts="contacts">
       		<li class="titles titles-{{!showContacts}}" ng-click="showContacts=!showContacts;">
	            {{group.name}}
	        	<span>({{group.membercount}})</span>
	        </li>
			<li ng-show="showContacts">
				<a class="friendDetail" ng-repeat="contact in contacts" ng-click="personChat(contact)">
			  		<span class="avatar" style="float: left;">
			  			<img ng-if="contact.avatar" ng-src="{{contact.avatar}}">
			  			<img ng-if="!contact.avatar" src="/images/oneps40.png">
			  		</span>
			  		<div class="left name" style="">{{contact.name}}</div>
			  			<p class="mystar" style="display:none"></p>
			        <div class="clr"></div>
			  	</a>
			</li>
       	</group>
    </div>
</organization>